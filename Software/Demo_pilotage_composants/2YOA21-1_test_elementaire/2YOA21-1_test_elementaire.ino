// Capteur de présence avec un capteur de distance SHARP 2Y0A21
// Auteur : JLC - 2023/11/14
//
// Ce programme permet de visualiser sur le moniteur série la tension
// délivrée par le capteur de distance SHARP 2Y0A21
// Tu peux trouver la valeur qui sépare les deux situations que tu cherches à distinguer,
// par exmple :
// - capteur libre de tout obstacle,
// - obstacle posé sur le capteur.
//
// En injectant cette valeur dans le programme 2YOA21-2_detecteur_presence.ino, 
// tu peux vérifier que le capteur permet bien de détecter la présence d'un objet.
//

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  // Le capteur doit être connecté sur l'entrée analogique A0:
  int v_num = analogRead(A0);
  
  String mess("Vin: ");
  mess += String(v_num);
  
  Serial.println(mess);

  delay(500);
}
