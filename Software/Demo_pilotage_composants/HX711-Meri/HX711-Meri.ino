//###############################################//
// projet "Balance connectée pour ruche"
//
// ENSAM 2022-2023  v1.0
// 2023-04-03: affichage masse avec 2 chiffres
//
//###############################################//

#include "HX711.h"

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define calibration_factor 997155 // calibration factor for MERI sensor

#define DOUT  3
#define CLK  2

LiquidCrystal_I2C lcd(0x20,16,2);  // set the LCD address to 0x20 for a 16 chars and 2 line display

// Create the object scale, of type HX711:
HX711 scale;


void setup()
{  
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  lcd.setBacklight(HIGH);
   
  lcd.home();
  
  lcd.setCursor(0, 0);
  lcd.print("Connected scale ");
  delay(1000);
  
  scale.begin(DOUT, CLK);
  
  lcd.setCursor(0, 0);
  lcd.print("Calibrating... ");
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  delay(1000);

  lcd.setCursor(0, 0);
  lcd.print("Taring...      ");
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  delay(1000);

  lcd.setCursor(0, 0);
  lcd.print("Ready...       ");
}

void loop()
{  
  float m = scale.get_units(10);
  if (m < 0.) m=0.; 
  String M(m*1000,0);  
  
  lcd.setCursor(0,0);
  lcd.print("M: ");
  lcd.print(M);
  lcd.print("  g       ");

  Serial.print(M.c_str()); Serial.println(" g");
  
  delay(500);
}
