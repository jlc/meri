/*
  Démo utilisation de Adafruit IR_Breakbeam_sensor 2167
*/

#define LEDPIN 13

#define SENSORPIN 2

// variables will change:
int sensorState = 0, lastState=0;

void setup() 
{
  // initialize the LED pin as an output:
  pinMode(LEDPIN, OUTPUT);
  
  // initialize the sensor pin as an input:
  pinMode(SENSORPIN, INPUT_PULLUP);
  
  Serial.begin(9600);
}

void loop()
{
  // read the state of the pushbutton value:
  sensorState = digitalRead(SENSORPIN);

  //
  // 1. Exemple de traitement qui utilise l'ÉTAT du faiceau :
  //
  
  if (sensorState == LOW) 
  {
    // Éteindre la LED orange de l'Arduino UNO si la barrière est coupée:
    digitalWrite(LEDPIN, LOW);
  }
  else 
  {
    // Allumer la LED orange de l'Arduino UNO si la barrière n'est pas coupée
    digitalWrite(LEDPIN, HIGH);
  }
  
  
  //
  // 2. Exemple de traitement qui sera fait une seule fois au changment d'état 
  //    de la barrière (coupée / pas coupée)
  //

  if (sensorState == LOW && lastState == HIGH)
  {
    Serial.println("Faisceau coupé");
  }
  else if (sensorState == HIGH && lastState == LOW)
  {
    Serial.println("Faisceau pas coupé");
  } 
  
  lastState = sensorState;
  delay(200);
}
