//
// Theo Pariunello & Jean-Luc Charles
//
// MERI - 2023-03-09 v1.1
//

//
// Code de demo pour le pilotage d'un moteur pas à pas avec le ciccuit 
// "AdaFruit Motor Shield shield  v2.3" qui permet de piloter 2 moteurs.
//

#define STOP_PROGRAMM while (1) {;}

#include <math.h>
#include <Adafruit_MotorShield.h>

// Création de l'objet shield, de type Adafruit_MotorShield:
Adafruit_MotorShield shield = Adafruit_MotorShield();

// shield.getStepper(nb_step_per_revol, port) renvoie l'objet pilote du moteur pas à pas :
// 'nb_step_per_revol' : nombre de pas par tour du moteur PAP
// 'nb_step_per_revol' : nombre de pas par tour du moteur PAP
// 'port' : indique sur quel port le moteur pas à pas est branché 
//    moteur PAP branché sur M1 & M2 : port -> 1
//    moteur PAP branché sur M3 & M4 : port -> 2

const int nb_step_per_revol = 200;
const float degre_per_step = 360./nb_step_per_revol;

Adafruit_StepperMotor * myMotorPAP_2 = shield.getStepper(nb_step_per_revol, 1);

// relâcher le couple quand le moteur est à l'arrêt sans
// quoi ses bobines vont chauffer :

       

void setup() 
{
  Serial.begin(9600);               // démarrer  la liaison USB à 9600 bps
  while (!Serial) {;}               // attendre que la liaison réponde
  Serial.println("Stepper test");   // un petit message d'accueil...

  if (!shield.begin()) 
  {
    // créer avec la fréquence par défaut 1.6KHz
    // if (!AFMS.begin(1000)) { // OU avec une fréquence différente, comme 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1) {;}                  // bloquer la suite
  }
  Serial.println("Motor Shield found.");
  myMotorPAP_2->setSpeed(60);    
}

void loop() 
{
  Serial.println("==================================");
  Serial.println("= 200 pas (un tour) à 1/2 tour/sec =");
  Serial.println("==================================");
  
  myMotorPAP_2->setSpeed(30);   // 30 tours / min => 1/2 tour / sec
  myMotorPAP_2->step(200, FORWARD, DOUBLE);
  myMotorPAP_2->release();
  delay(3000);  

  Serial.println("==================================");
  Serial.println("= 200 pas (un tour) à 2 tour/sec =");
  Serial.println("==================================");
  
  myMotorPAP_2->setSpeed(120);  // 120 tours / min =>  tours / sec
  myMotorPAP_2->step(200, FORWARD, DOUBLE);
  myMotorPAP_2->release();
  delay(3000);  

  Serial.println("==========================================");
  Serial.println("= 90° dans un sens puis 09° dans l'autre =");
  Serial.println("==========================================");

  myMotorPAP_2->setSpeed(60);   // 1 tour / sec
  int nb_step = round(90/degre_per_step);
  myMotorPAP_2->step(nb_step, FORWARD, DOUBLE);
  delay(1000);  
  myMotorPAP_2->step(nb_step, BACKWARD, DOUBLE);
  myMotorPAP_2->release();    
  
  delay(3000);
}

// fonction step(#steps, direction, step_type)
//  '#steps'    : nombre de pas que le moteur doit faire
//  'direction' : sens de rotation FORWARD (avant); BACKWARD (arrière)
//  'step_type' : type de pas à utiliser 
//                SINGLE -> activation d'une seule bobine moteur (minimum de puissance)
//                DOUBLE -> 2 bobines alimenteés en une seule fois (plus de couple)
//                INTERLEAVE -> doubler la resoluton : plus de vitesse
//                MICROSTEP  -> créer un mouvement fluide entre les pas : plus de précision

// release() permet de débloquer le maintient en positon par defaut après avoir terminé son pas
// la fonction "release()" relache les bobines afin qu'elles puissent tourner librement
// aucun courant ne circule dans le moteur 
