// Capteur de présence avec un capteur de distance SHARP 2Y0A21
// Auteur : JLC - 2022/11/14
//
// Ce programme fait suite au programme 2YOA21-1_test_elementaire.ino 
// où tu as pu visualiser sur le moniteur série la tension délivrée par 
// le capteur de distance 2Y0A21 afin de déterminer une valeur seuil 
// qui sépare les deux états que tu veux distinguer, par exemple :
// - capteur libre de tout obstacle,
// - obstacle posé sur le capteur.
//
// Il faut scruter le message ""objet détecté ..." dans le moniteur série pour 
// voir sur ton programme fonctionne bien...

// Définit ici la valeur seuil de la tension du 2Y0A21 que tu veux tester:
const int valeur_seuil = 100;

int compteur = 0;

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  // Le capteur doit être connecté sur l'entrée analogique A0:
  int v_num = analogRead(A0);

  if (v_num > valeur_seuil)
  {
    compteur++;
    String mess("objet détecté ");
    mess += String(compteur);
²    Serial.println(mess);
  }
  
  delay(500);
}
