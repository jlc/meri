//
// Plateforme MERI
// Automate à États Finis - Slave Intermédiaire - Braccio++
// WP4000_ContrôleMasse
//
// v1.0 2023-03-10 Jean-Luc Charles & Théo Parrinello : version initiale
// v1.1 2023-03-28 JLC : nouvelles positions de travail + tempo 10s + INPUT_PULLUP
// v1.2 2024-03-27 JLC : entrée balance sur pin 10 (la pin13 gêne le display)
//

///////////////
// IMPORTANT //
///////////////
// Sur les Braccio++ le entrées D2,D3,D45 et A0, A1 sont utilisées par 
// le joystick du obot:
// BTN_DOWN  -> 2
// BTN_LEFT  -> 3
// BTN_RIGHT -> 4
// BTN_UP    -> 5
// BTN_SEL   -> A0
// BTN_ENTER -> A1
//
// Le bus SPI utilise les ports 11, 12 et 13 :
// SPI-CIPO -> D12
// SPI-COPI -> D11
// SPI-SCK  -> D13
//////////////

/**********************************************************************
  Les includes nécessaires au programme  
 **********************************************************************/

// 'Braccio++.h' : gestion du bars robotisé Braccio++
#include <Braccio++.h>

// 'WiFiNANA.h' : accès à la LED RBG du Braccio++
#include <WiFiNINA.h>

// "Wire": gestion du bus I2C:
#include <Wire.h>


/**********************************************************************
  Définition des Macros utiles : états automate, numéros des broches...
 **********************************************************************/
#define BUTTON_ENTER 6
#define TIME_DELAY 1500

#define INIT 1
#define WAKE_UP 2
#define WAIT 3
#define RUN  4

// la broche de liaison avec l'Arduino Balance
# define pinBALANCE 10

///////////////
// IMPORTANT //
///////////////
// 
// On utilise les I/O 3, 4 & 5 qui sont nomalement utilisées
// pour le Joystick du Braccio++ : 
//
// !!! ATTENTION !!! il ne faut pas manipuler le Joystick du Bracccio
//                   sous peine de griller les E/S du RP2040 !!!
//////////////

#define pinReadMaster  3
#define pinWriteMaster 4
#define pinReadSlave   6
#define pinWriteSlave  5

/**********************************************************************
  Déclaration/définition des variables globales 
 **********************************************************************/

// Événements gérés par l'automate
bool Master_GO  = false;
bool Slave_OK   = false;
bool Event_BPA  = false;
bool Event_DONE = false;

int state;                 // L'ETAT de l'automate
int prev_BP_ENTER_state;   // mémorisation état bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int oldReceivedFromSlave;  // Mémorisation du signal émis par Slave
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

int etat_LEDG = 0;

lv_obj_t* label;
String mess;
lv_style_t style;

//
// Braccio ++ joints
//
auto gripper    = Braccio.get(1);
auto wristRoll  = Braccio.get(2);
auto wristPitch = Braccio.get(3);
auto elbow      = Braccio.get(4);
auto shoulder   = Braccio.get(5);
auto base       = Braccio.get(6);

//
// Les positions du bras robotisé
//

// Déclaration du type "six_angle" : tableau de 6 flots
typedef float six_angles[6];

// Position de sécurité, bras vertical:
const six_angles SAFE_POS = {157.5, 157.5, 157.5, 157.5, 157.5, 90.0};

// Position "prêt à travailler" (Cobra):
const six_angles WAIT_POS = {160.0, 160.0, 210.0, 240.0, 100.0, 180.0};

//============== Position de travail ========================//
const six_angles WORK_POS1[] = {\
 {211.68, 95.84, 166.79, 163.56, 161.60, 263.81},
 {211.68, 95.84, 189.08, 264.44, 133.24, 263.81},
 {129.39, 95.84, 189.24, 264.76, 133.56, 263.81},
 {129.70, 95.84, 211.37, 226.01, 140.10, 259.88},
 {129.70, 95.84, 211.05, 226.01, 138.76, 232.86},
 {129.70, 95.84, 211.05, 226.33, 138.60, 211.05},
 {129.70, 95.84, 211.05, 227.35, 138.76, 186.01},
 {129.70, 98.12, 199.79, 265.55, 123.48, 178.53},
 {129.70, 98.12, 134.58, 265.39, 176.87, 178.84},
 {211.05, 96.63, 135.53, 265.07, 178.21, 178.68},
 {211.05, 96.63, 135.53, 265.23, 138.76, 178.84},
 {-1}};

//////////////////////////////////////////////////////
const six_angles WORK_POS2[] = {\
 {211.05, 87.33, 120.88, 265.07, 131.04, 182.38},
 {211.05, 87.33, 120.88, 265.07, 189.87, 182.38},
 {130.41, 87.33, 120.88, 265.39, 190.89, 182.38},
 {130.65, 87.33, 189.87, 264.44, 129.70, 182.38},
 {130.65, 87.33, 191.21, 264.76, 120.25, 109.46},
 {130.65, 87.33, 189.55, 262.79, 123.01, 42.76},
 {130.65, 87.33, 176.87, 200.65, 198.06, 42.60},
 {211.37, 90.64, 171.36, 205.54, 198.69, 42.60},
 {211.05, 90.72, 162.23, 167.11, 163.17, 43.08},
 {-1}};

//////////////////////////////////////////////////////

const six_angles WORK_POS3[] = {\
{211.68, 95.84, 166.79, 163.56, 161.60, 263.81},
 {211.68, 95.84, 189.08, 264.44, 133.24, 263.81},
 {129.39, 95.84, 189.24, 264.76, 133.56, 263.81},
 {129.70, 95.84, 211.37, 226.01, 140.10, 259.88},
 {129.70, 95.84, 211.05, 226.01, 138.76, 232.86},
 {129.70, 95.84, 211.05, 226.33, 138.60, 211.05},
 {129.70, 95.84, 211.05, 227.35, 138.76, 186.01},
 {129.70, 98.12, 199.79, 265.55, 123.48, 178.53},
 {129.70, 98.12, 134.58, 265.39, 176.87, 178.84},
 {211.05, 96.63, 135.53, 265.07, 178.21, 178.68},
 {211.05, 96.63, 135.53, 265.23, 138.76, 178.84},
 {-1}};

//////////////////////////////////////////////////////

//================= Fin positions ===========================//

/*
//============== Position de travail ========================//
const six_angles WORK_POS[] = {\
 {158.68, 158.84, 209.71, 269.33, 152.30, 183.72},
 {158.68, 157.97, 209.08, 267.67, 114.03, 183.72},
 {158.68, 157.50, 175.93, 267.99, 114.35, 183.72},
 {211.05, 159.00, 174.59, 268.70, 125.76, 229.24},
 {210.89, 157.50, 118.99, 270.27, 193.17, 229.24},
 {152.62, 159.31, 119.78, 270.11, 192.86, 229.24},
 {152.77, 158.29, 231.21, 211.99, 160.88, 229.24},
 {152.77, 157.66, 171.04, 254.68, 141.75, 186.01},
 {152.77, 157.34, 104.42, 270.43, 195.06, 186.01},
 {211.68, 172.30, 104.26, 270.27, 194.43, 186.01},
 {143.32, 170.34, 262.79, 256.10, 121.20, 186.01},
 {-1}};
//================= Fin positions ===========================//
*/

/**********************************************************************
   Déclaration des fonctions utiliées dans le programme 
   (la définition est codée à la fin du programme)
 **********************************************************************/
void init_LSC_screen(void) ;
void update_message(const char* message);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur: 
  Serial.begin(9600);

  // Initialisation de l'écran du Braccio:
  if (!Braccio.begin(init_LSC_screen)) 
  {
    update_message("Error Braccio");
    for (;;) {}
  }

  // initialization variables:
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
  prev_BP_ENTER_state   = 0;

  //
  // Configuration des E/S numériques
  //
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);

  // Ligne numérique avec l'Arduino de la balance
  pinMode(pinBALANCE, INPUT_PULLUP);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  

  // Temporisation démarrage
  update_message("Waiting 10s");
  delay(10000);

  // mettre le bras robotisé en position SAFE (aligné vertical):
  Braccio.moveTo(SAFE_POS[0], SAFE_POS[1], SAFE_POS[2], SAFE_POS[3], SAFE_POS[4], SAFE_POS[5]);
  delay(3000);

  //
  // L'automate démarre dans l'état INIT
  //  
  state = INIT;
  tempo = 100;    //ms, pour clignotement rapide
  update_message("INIT");

  // éteindre la LED RB:
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
  
  delay(1000);  
}

void loop() 
{  
  //
  // 1 - lecture des périphériques qui peuvent fournir un événement
  //     susceptible de provoquer un changement d'état de l'automate.…   

  int receivedFromMaster = digitalRead(pinReadMaster);
  int receivedFromSlave  = digitalRead(pinReadSlave);
  int cur_BP_ENTER_state = Braccio.isButtonPressed_ENTER();
  
  // DEBUG_JLC: Serial.println("prev_BP: " + String(prev_BP_ENTER_state) + " - curr_BP: " + String(cur_BP_ENTER_state));

  //
  // 2 - Traitement des événements entraînant une transition d'état
  //     (cf Tableau des transitions d'état)

  // Événement Event_PBA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (prev_BP_ENTER_state  == LOW) && (cur_BP_ENTER_state == HIGH);

  // Master:GO 
  // L'automate slave-Intermédiaire reçoit un front descendant venant de l'automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);  

  // Slave:OK 
  // L'automate Slave-Intermédiaire reçoit un front descendant venant de l'automate-Aval)
  Slave_OK = (oldReceivedFromSlave  == HIGH) && (receivedFromSlave  == LOW);
  
  if (Master_GO)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAKE_UP();
      break;

      case WAIT:
        // Master:GO est le même événement que Transport:GO
        change_to_state_RUN();
      break;
    }
  }
  else if (Slave_OK)
  {
    switch (state)
    {
      case WAKE_UP:
        change_to_state_WAIT();
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH
      break;
    }
  }    
  else if (Event_BPA)
  {
    switch (state)
    {
      case INIT:
        change_to_state_WAKE_UP();
      break;

      case WAKE_UP:
        change_to_state_WAIT();
      break;

      case WAIT:
        change_to_state_RUN();        
      break;
    }
  }    
  else if (Event_DONE)
  {
    switch (state)
    {
      case RUN:
        // désactiver le signal DONE:
        Event_DONE = false;
        change_to_state_WAIT();
      break;
    }
  }    

  //
  // 3 - Traitement des états
  //
  
  switch (state) 
  {
    case INIT:
      // clignotement LED verte
      etat_LEDG = 1 - etat_LEDG;
      digitalWrite(LEDG, PinStatus(etat_LEDG));
    break;

    case WAKE_UP:
      // clignotement LED verte
      etat_LEDG = 1 - etat_LEDG;
      digitalWrite(LEDG, PinStatus(etat_LEDG));
    break;
        
    case WAIT:
      // RAF
      ;
    break;

    case RUN:
      // Faire faire au bras les mouvements:
      delay(2000);
      int i = 0;
      while (*WORK_POS1[i] != -1)
      {
        const six_angles & angles = WORK_POS1[i++];
        Braccio.moveTo(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5]);
        delay(1000);
      }

      delay(2000); 
      if (digitalRead(pinBALANCE) == LOW)
      { 
        i = 0;
        Serial.println("Masse pas OK -> mouvement WORK_POS2");
        while (*WORK_POS2[i] != -1)
        {
          const six_angles & angles = WORK_POS2[i++];
          Braccio.moveTo(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5]);
          delay(1000);
        }
      }
      else 
      { 
        // LA masse mesurée est OK
        i = 0;
        Serial.println("Masse OK -> mouvement WORK_POS3");
        while (*WORK_POS3[i] != -1)
        {
          const six_angles & angles = WORK_POS3[i++];
          Braccio.moveTo(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5]);
          delay(1000);
        }
        
      }    

      // envoyer le signal DONE à l'automate:
      Event_DONE = true;
    break;
  }
  
  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldReceivedFromSlave  = receivedFromSlave;
  prev_BP_ENTER_state   = cur_BP_ENTER_state;
  
  delay(tempo);
}

inline void change_to_state_WAKE_UP()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state= WAKE_UP;

  // éteindre la LED, elle clignotera
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
  
  // Affichage sur LCD:
  update_message("WAKE_UP");
          
  // Send a GO to the next slave:
  delay(1000);
  digitalWrite(pinWriteSlave, LOW);
  delay(tau); // ms        
  digitalWrite(pinWriteSlave, HIGH);  // keep the line HIGH

  // remettre le clignotement à 1Hz
  tempo = 500;
}

inline void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state = WAIT;

  // Allumer LED verte:
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, HIGH);
  digitalWrite(LEDB, LOW);
  
  // Affichage sur LCD:
  update_message("WAIT");

  // Mettre le bras en position d'attente (Cobra):
  Braccio.moveTo(WAIT_POS[0], WAIT_POS[1], WAIT_POS[2], WAIT_POS[3], WAIT_POS[4], WAIT_POS[5]);
  delay(2000);
  
  // tempo sans clignotement
  tempo = 300;
}

void change_to_state_RUN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état RUN
  //

  state = RUN;

  // Affichage sur LCD:
  update_message("RUN");

  // Allumer LED rouge:
  digitalWrite(LEDR, HIGH);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);

  // tempo sans clignotement          
  tempo = 300;
}

void init_LSC_screen(void) 
{
  //
  // Initialisation de l'écran du Braccio++
  //
  Braccio.lvgl_lock();
  lv_style_init(&style);
  lv_style_set_text_font(&style, &lv_font_montserrat_32);  //lv_font_montserrat_48);
  label = lv_label_create(lv_scr_act());
  Braccio.lvgl_unlock();
}

void update_message(const char* message) 
{
  //
  // écrire un mssage sur l'écran du Braccio++
  //
  Braccio.lvgl_lock();
  mess = message;
  lv_label_set_text_static(label, mess.c_str());
  lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);
  lv_obj_add_style(label, &style, LV_PART_MAIN);
  Braccio.lvgl_unlock();
}
