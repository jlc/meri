//
//
// Plateforme MERI
// Automate à États Finis - Slave Intermédiaire
// WP6000_Recyclage
// 

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT    1
#define WAKE_UP 2
#define WAIT    3
#define WARM    4
#define COOLING 5

#define pinLEDV  7
#define pinLEDR  8
#define pinBP1   2
#define pinRELAIS 9

#define pinReadMaster  3
#define pinWriteMaster 4
#define pinReadSlave   6
#define pinWriteSlave  5

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
// Événements gérés par l'automate
bool Master_GO  = false;
bool Slave_OK   = false;
bool Event_BPA  = false;
bool Event_DONE = false;

const bool debug = false;

int state;                 // L'ETAT de l'automate
int oldStateBP1;           // Mémorise l’état du bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int oldReceivedFromSlave;  // Mémorisation du signal émis par Slave
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

//
// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
//
LiquidCrystal_I2C lcd(0x20,16,2);    

// Les variables globales propres au WP
// 
const int temps_refroid_sec = 10;     // durée de refroidissement : ajuster en fonction des besoins
const int temps_chauffe_sec = 10;    // durée de chauffe : ajuster en fonction des besoins
unsigned long int temps_sec = 0;     // compteur de temps écoulé
unsigned long int t0_start_chauffe_ms;  // top chrono début chauffe
unsigned long int t0_start_refroidissement_ms;  // top chrono début refroidissmeent

 
/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();
void change_to_state_COOLING();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
  
  //
  // initialisation des variables globales:
  //
  oldStateBP1 = LOW; 
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  pinMode(pinBP1, INPUT);
  pinMode(pinRELAIS, OUTPUT);
  
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // Temporisation démarrage
  tempo_LCD(2);

  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide

  // éteindre les LEDs:
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, LOW);

  // Affichage sur LCD:
  LCD_display("INIT", 0);
  
  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int stateBP1 = digitalRead(pinBP1);
  int receivedFromMaster = digitalRead(pinReadMaster);
  int receivedFromSlave  = digitalRead(pinReadSlave);

  if (debug)
  {
    String mess = "From master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "From Slave : " +String(oldReceivedFromSlave) + " " + String(receivedFromSlave);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStateBP1) + " " + String(stateBP1);
    Serial.println(mess);
    Serial.println("");
  }
  
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement BPA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStateBP1 == LOW) && (stateBP1 == HIGH); 

  // Master:GO (Slave-Intermédiaire reçoit un front descendant venant de Automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);

  // Slave_OK (Slave-Intermédiaire reçoit un front descendant venant de Automate-Aval)
  Slave_OK  = (oldReceivedFromSlave  == HIGH) && (receivedFromSlave  == LOW);

  if (Master_GO)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAKE_UP();
      break;
    }
  }
  else if (Slave_OK)
  {
    switch (state)
    {
      case WAKE_UP:           
        change_to_state_WAIT();
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH
      break;

      case WAIT:
        // Salave:OK <=> Transport:GO
        change_to_state_WARM();
      break;
    }
  }
  else if (Event_DONE)
  {
    switch(state)
    {
      case WARM:
        change_to_state_COOLING();
      break;

      case COOLING:
        change_to_state_WAIT();
      break;
    }
    Event_DONE = false;
  }
  else if (Event_BPA)
  {
    switch (state)
    {
      case INIT:
        change_to_state_WAKE_UP();
      break;
      
      case WAKE_UP:
        change_to_state_WAIT();
      break;
      
      case WAIT:
        change_to_state_WARM();
      break;
      
      case WARM:
        change_to_state_COOLING();
      break;

      case COOLING:
        change_to_state_WAIT();
      break;
    } 
  }


  //
  // 3 - Traitement des états
  //

  
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAKE_UP:
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
     ;    // RAF
    break;

    case WARM:
    {
      // Le relais a été allumé à la transition WAIT->WARM
      temps_sec = int((millis() - t0_start_chauffe_ms)/1e3);
      if (temps_sec > temps_chauffe_sec)
      {
        // La durée de chauffe est atteinte, éteindre le relais:
        digitalWrite(pinRELAIS, LOW);
        LCD_display("STOP Chauffe", 1);
        delay(1000);  // Ajout d'une petite pause pour assurer l'affichage  
        
        // générer l'événement DONE:
        Event_DONE = true;
      }
      else
      {
        // afficher le cumul de temps de chauffe sur LCD:
        String duration_min = String(int(temps_sec/60.));
        String duration_sec = String(int(temps_sec));
        LCD_display("Chauffe: " + duration_sec, 1);
        delay(500);
      }
    }
    break;

    case COOLING:
    {
      temps_sec = int((millis() - t0_start_refroidissement_ms)/1e3);
      if (temps_sec > temps_refroid_sec)
      { 
        LCD_display("Fin Refroidiss.", 1);
        
        // générer l'événement DONE:
        Event_DONE = true;
      }
      else
      {
        // faire clignoter la LEDR
        digitalWrite(pinLEDR, 1 - digitalRead(pinLEDR));
        
        // le relais a déjà été éteint      
      
        // afficher le cumul de temps de refroidissement sur LCD:
        String duration_min_re = String(int(temps_sec/60.));
        String duration_sec_re = String(int(temps_sec));
        LCD_display("Refroid.: " + duration_sec_re, 1);
        delay(500);
      }
    }
    break;
  }

  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldReceivedFromSlave  = receivedFromSlave;
  oldStateBP1 = stateBP1;
 
  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //
  
  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

//
//Actions à faire aux transitions d'état
//


inline void change_to_state_WAKE_UP()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state= WAKE_UP;

  // éteindre LED verte
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("WAKE_UP", 0);
  LCD_display("", 1, false);
  delay(1000);
          
  // Envoyer un GO à l'Automate-Aval
  digitalWrite(pinWriteSlave, LOW);
  delay(tau); // ms
  digitalWrite(pinWriteSlave, HIGH);  // keep the line HIGH

  // remettre le clignotement à 1Hz
  tempo = 500;
}

void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state = WAIT;

  // Allumer/éteidre les LEDs
  digitalWrite(pinLEDV, HIGH);
  digitalWrite(pinLEDR, LOW);
  
  // Affichage sur LCD:
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(500);
  
  // tempo sans clignotement
  tempo = 300;
}

void change_to_state_WARM()
{
    // 
  // Actions à toujours faire pour la transition vers l'état RUN
  //
  state = WARM;

  // éteindre LEDV, allumer LEDR
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, HIGH);
  
  // Affichage sur LCD:
  LCD_display("RUN", 0);
  LCD_display("", 1, false);
  delay(500);

  // Allumer le relais...
  digitalWrite(pinRELAIS, HIGH); 
  // Initialiser le décompte...
  t0_start_chauffe_ms = millis(); // nbre de ms écoulées depuis le lancement du prog
  LCD_display("Start Chauffe", 1);
  
  // tempo chauffage          
  tempo = 500;
}

void change_to_state_COOLING()
{
    // 
  // Actions à toujours faire pour la transition vers l'état COOLING
  //
  state = COOLING;

  digitalWrite(pinRELAIS, LOW);
  digitalWrite(pinLEDR, LOW);  
  
  // Affichage sur LCD:
  LCD_display("COOLING", 0);
  LCD_display("", 1, false);
  delay(500);

  // Initialiser le décompte...
  t0_start_refroidissement_ms = millis(); // nbre de ms écoulées depuis le lancement du prog
  LCD_display("Start refroi", 1);

  // tempo cooling
  tempo = 500;
}
