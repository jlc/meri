//
// Plateforme MERI
// Automate à États Finis Master du WP2300_Refroidissement
//

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// "Adafruit_MotorShield" pour piloter un moteur pas à pas
#include <Adafruit_MotorShield.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAKE_UP 2
#define RUN 3
#define STOP 4

#define pinReadWP2100   3
#define pinWriteWP2100  2
#define pinReadWP2200   5
#define pinWriteWP2200  4
#define pinReadWP2400   7
#define pinWriteWP2400  6
#define pinReadWP6000   9
#define pinWriteWP6000  8

#define pinLEDV 14
#define pinLEDR 15
#define pinLEDJ 16
#define pinLEDB 17
#define pinBP   A14

#define Relais 10
#define ILS 19 

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
const bool debug = false;

int state;                 // L'ETAT de l'automate
int oldStatePB1;           // Mémorisation de l'état du Bouton Poussoir (BP)
int oldReceivedFromWP2100; // Mémorisation du signal émis par Slave WP2100
int oldReceivedFromWP2200; // Mémorisation du signal émis par Slave WP2200
int oldReceivedFromWP2400; // Mémorisation du signal émis par Slave WP2400
int oldReceivedFromWP6000; // Mémorisation du signal émis par Slave WP6000
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

bool Event_READY = false;
bool Event_ERROR = false;
bool Event_DONE  = false;

int pinReadSlave[]  = {pinReadWP2100, pinReadWP2200, pinReadWP2400, pinReadWP6000};
int pinWriteSlave[] = {pinWriteWP2100, pinWriteWP2200, pinWriteWP2400, pinWriteWP6000};
int timeout_delay[] = {1000, 1000, 5000, 1000};
String message[]    = {"Master:GO>WP2100", "Master:GO>WP2200", "Master:GO>WP2400", "Master:GO>WP6000"};

// Définition de la variable LCD_mess de type LCD_message
struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
LiquidCrystal_I2C lcd(0x20,16,2);  


Adafruit_MotorShield shield = Adafruit_MotorShield();

// shield.getStepper(nb_step_per_revol, port) renvoie l'objet pilote du moteur pas à pas :
// 'nb_step_per_revol' : nombre de pas par tour du moteur PAP
// 'port' : indique sur quel port le moteur pas à pas est branché 
//    moteur PAP branché sur M1 & M2 : port -> 1
//    moteur PAP branché sur M3 & M4 : port -> 2

const int nb_step_per_revol = 200;
const float degre_per_step  = 360./nb_step_per_revol;

Adafruit_StepperMotor *myMotor = shield.getStepper(nb_step_per_revol, 2);

// Les variables globale propres au WP
// 
bool ref_plateau = false;

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
bool WakeUP_Slave(int pinReadSlave, int pinWriteSlave, int timeoutDelay, String& mess, bool force_OK_true=false);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
   
  //
  // initialisation des variables globales:
  //
  oldStatePB1 = LOW; 
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  pinMode(pinLEDJ, OUTPUT);
  pinMode(pinLEDB, OUTPUT);

  pinMode(Relais, OUTPUT);
  pinMode(ILS, INPUT);
  pinMode(A14, INPUT);

  pinMode(pinBP, INPUT);
  pinMode(pinReadWP2100, INPUT_PULLUP);
  pinMode(pinWriteWP2100, OUTPUT);
  pinMode(pinReadWP2200, INPUT_PULLUP);
  pinMode(pinWriteWP2200, OUTPUT);
  pinMode(pinReadWP2400, INPUT_PULLUP);
  pinMode(pinWriteWP2400, OUTPUT);
  pinMode(pinReadWP6000, INPUT_PULLUP);
  pinMode(pinWriteWP6000, OUTPUT);

  // maintient des sorties Slave à HIGH:
  digitalWrite(pinWriteWP2100, HIGH);  
  digitalWrite(pinWriteWP2200, HIGH);  
  digitalWrite(pinWriteWP2400, HIGH);  
  digitalWrite(pinWriteWP6000, HIGH);  
  
  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // Temporisation démarrage
  tempo_LCD(2);
  
  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);

  // Initialiser le driver moteur PàP
  shield.begin();

  // Fixer la vitesse: 60 tours/mn -> 1 tour/sec
  myMotor->setSpeed(60); // en tour/mn

  // relâcher le couple quand le moteur est à l'arrêt 
  // sans quoi ses bobines vont chauffer :
  myMotor->release();

  // Mettre les realais en position ouverts
  digitalWrite(Relais, HIGH);
  
  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int statePB1 = digitalRead(pinBP);

  if (debug)
  {
    String mess = "BP         : "  + String(oldStatePB1) + " " + String(statePB1);
    Serial.println(mess);
    Serial.println("");
  }
    
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //
  
  // Événement Event_BPA (Bouton Poussoir Appuyé: front montant)
  bool Event_BPA = (oldStatePB1  == LOW) && (statePB1 == HIGH); 
  
  if (Event_BPA)  
  {
    switch (state)
    {
      case INIT: 
        // transition d'état
        state = WAKE_UP;   
        
        // Affichage sur LCD:
        LCD_display("WAKE_UP", 0);
                
        tempo = 500; //ms, pour clignotement à 1Hz      
      break;

      case WAKE_UP:
        Event_READY = true;  // simuler l'événement READY
        delay(5000);
      break;
    }
  }
  else if (Event_READY)  
  {
    switch (state)
    {
      case WAKE_UP:
        // transition d'état
        state = RUN;
        
        // Affichage sur LCD:
        LCD_display("RUN", 0);
        LCD_display("", 1, false);

        digitalWrite(pinLEDV, LOW);
        digitalWrite(pinLEDR, HIGH);
        digitalWrite(pinLEDJ, LOW);
        digitalWrite(pinLEDB, LOW);
        
        tempo = 300; //ms
      break;
    }
    Event_READY = false;
  }
  else if (Event_ERROR)
  {
    switch (state)
    {
      case WAKE_UP:
        // transition d'état
        state = STOP;
        
        // Affichage sur LCD:
        LCD_display("STOP", 0);
        LCD_display("Err event READY", 1, false);

        digitalWrite(pinLEDV, LOW);
        digitalWrite(pinLEDR, HIGH);
        digitalWrite(pinLEDJ, LOW);
        digitalWrite(pinLEDB, LOW);
        
        tempo = 300; //ms
      break;
    }
    Event_ERROR = false;    
  }

  //
  // 3 - Traitement des états de l'automate
  //
  
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;
    
    case WAKE_UP:    
      // attente avec timeout des Slave:OK des 4 chaînes d'automates
      bool SLAVE_OK;
      for (int i=0; i < 4; i++)
      {
        bool force_OK_true = true; // forcer le slave_OK en attendant que tous les automates soient branchés
        SLAVE_OK = WakeUP_Slave(pinReadSlave[i], pinWriteSlave[i], timeout_delay[i], message[i], force_OK_true);
        if (SLAVE_OK == false)
        {
          Event_ERROR = true;
          break;
        }
      }
      if (Event_ERROR) break;
      
      // Si on arrive ici, c'est que toutes les chaînes d'automates sont OK...
      Event_READY = true;
    break;

    case RUN:
      if (ref_plateau == false)
      {
        ref_plateau = true;
    
        Serial.println("Plateau en cours d'indexation");
        LCD_display("Ref plateau:", 1);
        
        myMotor->setSpeed(30); // en tour/mn
    
        while (true)
        {
          myMotor->step(1, FORWARD, DOUBLE);
          int ils_state = digitalRead(ILS);
          if(ils_state == HIGH)
          {
            Serial.println("Plateau indexé");
            LCD_display("Ref plateau: OK", 1);
            myMotor->step(0, FORWARD, DOUBLE); 
            myMotor->release();
            break;
          }
        }
      }
      delay(2000);
      Serial.println("moteur");
      LCD_display("Position secteur", 0);
      LCD_display(" ",1);
      // Faire tourner le moteur : 360 pas -> 360/200 = 1.8 tour
      myMotor->step(360, FORWARD, DOUBLE); 
    
      // relâcher le couple quand le moteur est à l'arrêt 
      // sans quoi ses bobines vont chauffer :
      myMotor->step(0, FORWARD, DOUBLE); 
      myMotor->release();
    
      // Allumer les ventilateurs:
      Serial.println("Allumage ventilateurs");
      LCD_display("Ventilateurs ON",1);
      digitalWrite(Relais, LOW);
    
      // Attendre un peu:
      delay(6000);
    
      // Éteindre les ventilateurs:
      Serial.println("Exteinction ventilateurs");
      LCD_display("Ventilateurs OFF",1);
      digitalWrite(Relais, HIGH); 
      
      delay(1000);

    break;

    case STOP:
      // Arrêter le programme
      while(true) {;}
    break;
  }

  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldStatePB1 = statePB1;
 
  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //

  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline bool WakeUP_Slave(int pinReadSlave, 
                         int pinWriteSlave, 
                         int timeout_delay,
                         String & message, 
                         bool force_OK_true)
{

  // faire clignoter la LEDV
  digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  

  // Envoi du signal GO (front descendant, durée 'tau') vers Slave:
  LCD_display(message, 1);
  digitalWrite(pinWriteSlave, LOW);
  delay(tau);  // ms
  digitalWrite(pinWriteSlave, HIGH);
  
  digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
  delay(tau);  // ms

  // attente du OK du Slave avec timeout 
  bool SLAVE_OK = false;
  int oldReceivedFromSlave = HIGH;
  unsigned int t0 = millis();
  while (millis() - t0 < timeout_delay)
  {
    int receivedFromSlave = digitalRead(pinReadSlave);
    SLAVE_OK = (oldReceivedFromSlave == HIGH) && (receivedFromSlave == LOW);

    digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    delay(tau);  // ms

    if (SLAVE_OK) break;
    oldReceivedFromSlave = receivedFromSlave;
  }
  if (force_OK_true) SLAVE_OK = true;
  return SLAVE_OK;
}
