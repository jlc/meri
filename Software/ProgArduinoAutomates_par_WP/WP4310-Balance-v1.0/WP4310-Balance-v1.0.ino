//
// Plateforme MERI ENSAM 2023-2024
// WP4310-Balance
// 

/**********************************************************************
  Les includes nécessaires au programme  
 **********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// "Adafruit_MotorShield" pour piloter un moteur pas à pas
#include <Adafruit_MotorShield.h>

// Include du HX-711
#include "HX711.h"

/**********************************************************************
  Définition des Macros utiles : numéros des broches...
 **********************************************************************/

#define calibration_factor 997155 // calibration factor for MERI sensor

#define DOUT  8
#define CLK   7
#define DEFAUT_PIN 13

/**********************************************************************
  Déclaration/définition des variables globales 
 **********************************************************************/

// Définition de la variable LCD_mess de type LCD_message
struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
LiquidCrystal_I2C lcd(0x20,16,2);  

// Create the object scale, of type HX711:
HX711 scale;

/**********************************************************************
   Déclaration des fonctions utiliées dans le programme 
   (la définition est codée à la fin du programme)
 **********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes, String mess = "");

void setup()
{  
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
 
  // Initialiser l'écran LCD
  lcd.init();
  lcd.backlight();
  lcd.setBacklight(HIGH);
   
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("Balance ready");
  delay(1000);
  
  // Démarrer la balance  
  LCD_display("Calibration", 0);
  LCD_display("", 1);

  scale.begin(DOUT, CLK);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  delay(1000);

  // Tarage Balance
  tempo_LCD(10, "Tarage ");

  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  delay(1000);
  
  LCD_display("Fait !", 1);
  delay(1000);

  //
  // Configuration des E/S numériques:
  //
  pinMode(DEFAUT_PIN, OUTPUT);
  digitalWrite(DEFAUT_PIN, HIGH);
}

void loop()
{  
  // Faire une mesure
  float m = scale.get_units(10);
  if (m < 0.) m=0.; 
  String M(m*1000, 0);  
  
  LCD_display("M: " + M + "  g", 0);

  if ( (m < 0.020) || m > 0.050)
  { 
    digitalWrite(DEFAUT_PIN, LOW);
    LCD_display("  >>> NOT OK <<<", 1);
  }
  else
  {
    digitalWrite(DEFAUT_PIN, HIGH);
    LCD_display("  >>>     OK <<<", 1);
  }  

  // attendre
  delay(1000);
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //

  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec, String message)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  if (message == "")
  {
    message = "Waiting ";
  }
  message += String(nb_sec) + " sec";

  LCD_display(message, 0);
  LCD_display("", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

