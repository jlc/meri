//
// Plateforme MERI
// Automate à États Finis - Slave Final 
// WP5000_TransportRebut
// 

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// "Adafruit_MotorShield" pour piloter un moteur pas à pas
#include <Adafruit_MotorShield.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAIT 2
#define RUN  3

#define pinLEDR 6
#define pinLEDV 7
#define pinPB1  8

#define pinReadMaster  3
#define pinWriteMaster 4

#define pin_capteur_IR_bas  A0
#define pin_capteur_IR_haut A1

#define seuil_IR_bas 500
#define seuil_IR_haut 490

#define STOP while (true) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
// Événements gérés par l'automate
bool Master_GO = false;
bool Event_BPA = false;
bool Event_BD  = false;

const bool debug = false;

int state;                 // L'ETAT de l'automate
int oldStatePB1;           // mémorise l’état du bouton poussoir PB1
int oldReceivedFromMaster; // mémorisation du signal émis par Master
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

// événement DONE
bool Event_DONE = false;

//
// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
//
LiquidCrystal_I2C lcd(0x20,16,2);  

struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

//
// Object driver du moteur Pas à Pas:
//
Adafruit_MotorShield shield = Adafruit_MotorShield();

// shield.getStepper(nb_step_per_revol, port) renvoie l'objet pilote du moteur pas à pas :
// 'nb_step_per_revol' : nombre de pas par tour du moteur PAP
// 'port' : indique sur quel port le moteur pas à pas est branché 
//    moteur PAP branché sur M1 & M2 : port -> 1
//    moteur PAP branché sur M3 & M4 : port -> 2

// Moteur pas à pas:
const int nb_step_per_revol = 200;
const float degre_per_step  = 360./nb_step_per_revol;

Adafruit_StepperMotor * myMotorPAP_2 = shield.getStepper(nb_step_per_revol, 2);

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
int mesure_capteur_IR(int pin_IR, int nb_mesure=10);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
   
  //
  // initialisation des variables globales:
  //
  oldStatePB1 = LOW; 
  oldReceivedFromMaster = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  
  pinMode(pinPB1, INPUT);
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  

  // Temporisation démarrage
  tempo_LCD(2);

  //
  // L'automate démarre dans l'état INIT
  //  
  state = INIT;
  tempo = 500; //ms, clignotement 1 Hz
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);
  delay(1000);

  // Initialiser le driver du moteur PàP
  if (!shield.begin()) 
  {
    LCD_display("ERR: Shield PAP", 1);
    STOP    // Arrêter là....
  }

  // Intialize the Shield Driver ADFRUIT:
  LCD_display("Shield PAP OK", 1);

  // Libérer couple moteur:
  myMotorPAP_2->release();          
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int statePB1   = digitalRead(pinPB1);
  int receivedFromMaster = digitalRead(pinReadMaster);

  // Lecture capteur présence bougie position basse
  int capteur_IR_bas = mesure_capteur_IR(pin_capteur_IR_bas, 10); 

  if (debug)
  {
    String mess = "From master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStatePB1) + " " + String(statePB1);
    Serial.println(mess);
    Serial.println("");
  }
  
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Master:GO (Slave-Intermédiaire reçoit un front descendant venant de Automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);
  
  // Événement Event_BPA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStatePB1  == LOW) && (statePB1 == HIGH); 
  
  if (Master_GO | Event_BPA)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAIT();
        // Send OK to previous Slave:
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH); 
     break;
    }
  }
  else if (capteur_IR_bas < seuil_IR_bas) 
  {
    // événement "bougie détectée" 
    
    LCD_display("Bougie en BAS", 1);
 
    switch(state)
    {
      case WAIT:
        change_to_state_RUN();        
      break;
    }
  }
  else if (Event_DONE)
  {
    switch(state)
    {
      case RUN:
        change_to_state_WAIT();     
      break;
    }
    Event_DONE = false;
  }
  
  //
  // 3 - Traitement des états
  //
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
      ; // RAF  
    break;

    case RUN:
      // make the job... 
      myMotorPAP_2->setSpeed(60);  // 60 tours / min => 1 tours / sec
      
      while (true)
      {
        myMotorPAP_2->step(10, BACKWARD, DOUBLE); 
  
        // lecture du capteur position haute:
        int capteur_IR_haut = mesure_capteur_IR(pin_capteur_IR_haut, 10);
        
        if (capteur_IR_haut < seuil_IR_haut) 
        {
          LCD_display("Bougie HAUTE", 1);
          break;
        }
      }
  
      // faire un tour de plus:
      myMotorPAP_2->step(1*200, BACKWARD, DOUBLE); 
      
      // libérer le couple:
      myMotorPAP_2->release();    

      //===========================//
      // générer l'événement DONE:
      //===========================//
      Event_DONE = true;
      
    break;
  }

  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldStatePB1 = statePB1;

  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //

  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline int mesure_capteur_IR(int pin_IR, int nb_mesure)
{
  //
  // faire la moyenne de "nb_mesure" valeurs du capteur IR
  //
  int valeur = 0;
  for (int i=0; i < nb_mesure; i++)
  {
    valeur += analogRead(pin_IR);
    delay(2);
  }
  return int(float(valeur) / float(nb_mesure));
}

inline void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state = WAIT;

  // éteindre/allumer les LEDS
  digitalWrite(pinLEDR, LOW);
  digitalWrite(pinLEDV, HIGH);
  
  // Affichage sur LCD
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(100);
  
  // tempo sans clignotement
  tempo = 300;
}

inline void change_to_state_RUN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state= RUN;
  
  // éteindre/allumer les LEDS
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, HIGH);

  // Affichage sur LCD:
  LCD_display("RUN", 0);
  LCD_display("Bougie en BAS", 1);

}
