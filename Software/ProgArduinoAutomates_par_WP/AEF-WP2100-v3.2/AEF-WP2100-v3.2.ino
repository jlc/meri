//
// Programme template pour un automate à états finis 
//
// Plateforme MERI - Automate Slave Final.
//
// v1.0 2023-03-10 Jean-Luc Charles & Théo Parrinello : version initiale
// v1.1 2023-03-27 JLC : mise en forme
// v1.2 2023-03-28 JLC : nouvelles positions de travail + tempo 10s + INPUT_PULLUP
//

///////////////
// IMPORTANT //
///////////////
// Sur les Braccio++ le entrées D2,D3,D45 et A0, A1 sont utilisées par 
// le joystick du obot:
// BTN_DOWN  -> 2
// BTN_LEFT  -> 3
// BTN_RIGHT -> 4
// BTN_UP    -> 5
// BTN_SEL   -> A0
// BTN_ENTER -> A1
//
// Le bus SPI utilise les ports 11, 12 et 13 :
// SPI-CIPO -> D12
// SPI-COPI -> D11
// SPI-SCK  -> D13
//////////////

#include <Braccio++.h>
#include <WiFiNINA.h>

//////////////////////////////////////////
// Les variables globales et les macros //
//////////////////////////////////////////
#define BUTTON_ENTER 6
#define TIME_DELAY 1500

#define INIT 1
#define WAIT 3
#define RUN  4

///////////////
// IMPORTANT //
///////////////
// 
// On utilise les I/O 3, 4 & 5 qui sont nomalement utilisées
// pour le Joystick du Braccio++ : 
//
// !!! ATTENTION !!! il ne faut pas manipuler le Joystick du Bracccio
//                   sous peine de griller les E/S du RP2040 !!!
//////////////

#define pinReadMaster  3
#define pinWriteMaster 4

#define fullStepAngle_deg 1.8
#define step_mode 1

int state;                 // L'ETAT de l'automate
int prev_BP_ENTER_state;   // mémorisation état bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

const int pinDir    = 5;   // pin para la dirección de rotación
const int pinStep   = 6;   // pin para el paso
const int pinEnable = 10;   // pin para Habilitar/Deshabilitar torque

const float stepAngle_deg = fullStepAngle_deg / step_mode;
const float stepAngle_rad = fullStepAngle_deg / step_mode * M_PI / 180.;
const int nbStepPerRevol  = int(360. / stepAngle_deg);
float speedRPS = 1;

int dir = 1;
bool DONE = false;

int etat_LEDG = 0;

lv_obj_t* label;
String mess;
lv_style_t style;

//
// Braccio ++ joints
//
auto gripper    = Braccio.get(1);
auto wristRoll  = Braccio.get(2);
auto wristPitch = Braccio.get(3);
auto elbow      = Braccio.get(4);
auto shoulder   = Braccio.get(5);
auto base       = Braccio.get(6);

//
// Les positions du bras robotisé
//

// Déclaration du type "six_angle" : tableau de 6 flots
typedef float six_angles[6];

// Position de sécurité, bras vertical:
const six_angles SAFE_POS = {211.37, 91.43, 167.11, 161.91, 161.28, 90};

// Position "prêt à travailler" (Cobra):
const six_angles WAIT_POS = {160.0, 160.0, 210.0, 240.0, 100.0, 180.0};


//============== Position de travail ========================//
const six_angles WORK_POS[] = {\

 {160.10, 158.68, 166.48, 148.92, 164.51, 173.64},
 {160.10, 158.68, 166.48, 252.39, 163.56, 173.64},
 {160.02, 159.00, 120.88, 252.71, 172.93, 173.64},
 {160.02, 159.00, 110.80, 252.71, 189.71, 173.64},
 {136.42, 159.00, 110.80, 252.71, 194.75, 173.64},
 {136.34, 159.00, 110.80, 242.63, 199.95, 173.64},
 {136.42, 159.00, 110.80, 241.84, 166.79, 173.64},
 {136.42, 159.00, 136.16, 221.13, 179.08, 133.24},
 {136.42, 159.00, 136.16, 236.41, 183.41, 133.24},
 {165.77, 159.00, 136.47, 236.41, 183.41, 133.24},
 {165.77, 159.00, 136.47, 219.87, 183.41, 133.24},
 {165.45, 163.88, 154.43, 157.82, 158.68, 133.40},
 {-1}};
//================= Fin positions ===========================//



/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/

void init_LSC_screen(void);
void update_message(const char* message);
void makeStepperTurn(int dir, float angle, float stepAngle, float nbRevolPerSec, bool releaseTorque = true);

void setup() 
{
  Serial.begin(115200);
  
  if (!Braccio.begin(init_LSC_screen)) 
  {
    update_message("Error Braccio");
    for (;;) {}
  }
  
  // initialization variables:
  oldReceivedFromMaster = HIGH;
    
  //
  // Configuration des E/S numériques
  //
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);

  // set pin for stepper motor
  pinMode(pinDir, OUTPUT);
  pinMode(pinStep, OUTPUT);
  pinMode(pinEnable, OUTPUT);

    
  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  

  // Temporisation démarrage
  update_message("Waiting 2");
  delay(2);

  // mettre le bras robotisé en position SAFE (aligné vertical):
  Braccio.moveTo(SAFE_POS[0], SAFE_POS[1], SAFE_POS[2], SAFE_POS[3], SAFE_POS[4], SAFE_POS[5]);
  delay(3000);

  //
  // L'automate démarre dans l'état INIT
  //  
  state = INIT;
  tempo = 100; //ms
  update_message("INIT");

  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
  
  prev_BP_ENTER_state = 0;

  digitalWrite(pinEnable, HIGH); // release motor torque
 
  Serial.println("Driver A4988 test begins...");
  Serial.print("Full step angle [°]: ");
  Serial.println(fullStepAngle_deg);
  Serial.print("Step mode          : 1/");
  Serial.println(step_mode);
  Serial.print("Step angle      [°]: ");
  Serial.println(stepAngle_deg);
  Serial.print("Step angle     [rd]: ");
  Serial.println(stepAngle_rad);
  Serial.print("nb step per revol. : ");
  Serial.println(nbStepPerRevol);

  pinMode(pinDir, OUTPUT);
  pinMode(pinStep, OUTPUT);
  pinMode(pinEnable, OUTPUT);

  digitalWrite(pinDir, LOW);
  digitalWrite(pinEnable, HIGH);
}


void loop() 
{
  //
  // 1 - lecture des périphériques qui peuvent fournir un événement
  //     susceptible de provoquer un changement d'état de l'automate.…   

  int receivedFromMaster = digitalRead(pinReadMaster);
  int cur_BP_ENTER_state = Braccio.isButtonPressed_ENTER();
  
  // DEBUG_JLC: Serial.println("prev_BP: " + String(prev_BP_ENTER_state) + " - curr_BP: " + String(cur_BP_ENTER_state));

  //
  // 2 - Traitement des événements entraînant une transition d'état
  //     (cf Tableau des transitions d'état)

  bool BPA       = ( prev_BP_ENTER_state  == LOW) && (cur_BP_ENTER_state == HIGH);
  bool Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);  
  if (Master_GO || BPA)
  {
    switch(state)
    {
      case INIT:
        // transition d'état      
        state= WAIT;

         // Allumer la LEDV
        digitalWrite(LEDR, LOW);
        digitalWrite(LEDG, HIGH);
        digitalWrite(LEDB, LOW);
        
        // Affichage sur LCD:
        update_message("WAIT");

        // Mettre le bras en position d'attente (Cobra):
        Braccio.moveTo(WAIT_POS[0], WAIT_POS[1], WAIT_POS[2], WAIT_POS[3], WAIT_POS[4], WAIT_POS[5]);
        delay(3000);
                
        // Send a GO to the Master:
        delay(1000);
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        
        // keep the line HIGH
        digitalWrite(pinWriteMaster, HIGH); 
        
        tempo = 500;
        prev_BP_ENTER_state = cur_BP_ENTER_state;

        digitalWrite(pinEnable, HIGH); // release motor torque
      break;
    }
  }

  BPA       = ( prev_BP_ENTER_state  == LOW) && (cur_BP_ENTER_state == HIGH);
  bool Master_OK  = (oldReceivedFromMaster  == HIGH) && (receivedFromMaster  == LOW);
  if (Master_OK || BPA)
  {
    switch (state)
    {
      case WAIT:
        // transition d'état            
        state = RUN;

        // Affichage sur LCD:
        update_message("RUN");

        // Allumer LED rouge:
        digitalWrite(LEDR, HIGH);
        digitalWrite(LEDG, LOW);
        digitalWrite(LEDB, LOW);

        tempo = 20;
        prev_BP_ENTER_state = cur_BP_ENTER_state;
      break;
    }
  }    


  if (DONE)
  {
    switch (state)
    {
      case RUN:
        // transition d'état            
        state = WAIT;

        // désactiver le signal DONE:
        DONE = false;

        // Affichage sur LCD:
        update_message("WAIT");

        // Allumer LED verte:
        digitalWrite(LEDR, LOW);
        digitalWrite(LEDG, HIGH);
        digitalWrite(LEDB, LOW);
        
        // Mettre le bras en position d'attente (Cobra):
        Braccio.moveTo(WAIT_POS[0], WAIT_POS[1], WAIT_POS[2], WAIT_POS[3], WAIT_POS[4], WAIT_POS[5]);
        delay(3000);

        digitalWrite(pinEnable, HIGH); // release motor torque
        
        tempo = 300;
      break;
    }
    prev_BP_ENTER_state = cur_BP_ENTER_state;
  }    

  //
  // 3 - Traitement des états
  //
  
  switch (state) 
  {
    case INIT:
      // clignotement LED verte
      etat_LEDG = 1 - etat_LEDG;
      digitalWrite(LEDG, PinStatus(etat_LEDG));

    break;
        
    case WAIT:
      ;
    break;

    case RUN:
      // Faire faire au bras les mouvements:
      delay(1000);
      int i = 0;
      while (*WORK_POS[i] != -1)
      {
        const six_angles & angles = WORK_POS[i++];
        Braccio.moveTo(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5]);
        delay(2000);
      }
      // envoyer le signal DONE à l'automate:
      DONE = true;

      Serial.print("\nSpeedRPS [RPS]: ");
      Serial.println(speedRPS);
      makeStepperTurn(dir, 22.5, stepAngle_deg, speedRPS, true);
      digitalWrite(pinEnable, HIGH); // release motor torque
      delay(1000);
     break;
  }
  
  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  
  prev_BP_ENTER_state   = cur_BP_ENTER_state;
  
  delay(tempo);
}

void makeStepperTurn(int dir, float angle, float stepAngle, float nbRevolPerSec, bool releaseTorque)
{
  // 
  // Pilotage d'un moteur pas à pas avec le driver A4988
  //
  // Arguments:
  // - dir : direction de rotation du moteur
  // - angle : angle de rotation en degrés
  // - stepAngle : angle en degré d'un pas moteur
  // - nbRevolPerSec : vitesse de rtaion, en nbre de tours par seconde
  // - releaseTorque ; true -> couper l'limentation meteur après avoir tourné,
  //                   false -> mainteint de l'alimentation (maintient couple)
  //
  const float nbRevol   = 360. / angle;
  const float timeDelay = 1000. / (nbRevolPerSec * nbStepPerRevol);
  const int totNbStep   = angle / stepAngle;

  Serial.print("RPS:");
  Serial.print(nbRevolPerSec);
  Serial.print(", timeDelay [ms]:");
  Serial.println(timeDelay);

  if (dir == 1)
  {
    digitalWrite(pinDir, LOW);
  }
  else
  {
    digitalWrite(pinDir, HIGH);
  }

  digitalWrite(pinEnable, LOW);

  for (int i = 0; i < totNbStep; i++)
  {
    digitalWrite(pinStep, HIGH);
    delayMicroseconds(2);
    digitalWrite(pinStep, LOW);
    delay(timeDelay);
  }
  if (releaseTorque) 
  {
    digitalWrite(pinEnable, HIGH);
  }
  else
  {
    digitalWrite(pinEnable, LOW);
  }
}


void init_LSC_screen(void) 
{
  //
  // Initialisation de l'écran du Braccio++
  //
  Braccio.lvgl_lock();
  lv_style_init(&style);
  lv_style_set_text_font(&style, &lv_font_montserrat_32);  //lv_font_montserrat_48);
  label = lv_label_create(lv_scr_act());
  Braccio.lvgl_unlock();
}

void update_message(const char* message) 
{
  //
  // écrire un message sur l'écran du Braccio++
  //
  Braccio.lvgl_lock();
  mess = message;
  lv_label_set_text_static(label, mess.c_str());
  lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);
  lv_obj_add_style(label, &style, LV_PART_MAIN);
  Braccio.lvgl_unlock();
}
