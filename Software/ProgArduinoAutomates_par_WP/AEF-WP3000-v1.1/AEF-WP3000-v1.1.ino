//
// Plateforme MERI
// Automate à États Finis Slave Intermédiaire du WP3000_Transport Bougie
// 

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// "Adafruit_MotorShield" pour piloter un moteur pas à pas
#include <Adafruit_MotorShield.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT    1
#define WAKE_UP 2
#define WAIT    3
#define RUN     4

#define pinLEDV 7
#define pinLEDR 8
#define pinBP1  A3

#define pinReadMaster  3
#define pinWriteMaster 4
#define pinReadSlave   6
#define pinWriteSlave  5

#define pin_capteur_IR_start  A0
#define pin_capteur_IR_IA     A1
#define pin_capteur_IR_masse  A2

#define STOP while (true) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/

// Événements gérés par l'automate
bool Event_BPA = false;
bool Master_GO = false;
bool Slave_OK = false;
bool Event_DONE = false;
bool Event_BD   = false;

const bool debug = false;

int state;                 // L'ETAT de l'automate
int stateBP1;              // état courant Bouton poussoir
int oldStateBP1;           // Mémorise l’état du bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int oldReceivedFromSlave;  // Mémorisation du signal émis par Slave
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant


struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
LiquidCrystal_I2C lcd(0x20,16,2);  

//
// Object driver du moteur Pas à Pas:
//
Adafruit_MotorShield shield = Adafruit_MotorShield();

// shield.getStepper(nb_step_per_revol, port) renvoie l'objet pilote du moteur pas à pas :
// 'nb_step_per_revol' : nombre de pas par tour du moteur PAP
// 'port' : indique sur quel port le moteur pas à pas est branché 
//    moteur PAP branché sur M1 & M2 : port -> 1
//    moteur PAP branché sur M3 & M4 : port -> 2

// Moteur pas à pas:
const int nb_step_per_revol = 200;
const float degre_per_step  = 360./nb_step_per_revol;

Adafruit_StepperMotor * myMotorPAP_2 = shield.getStepper(nb_step_per_revol, 2);

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
int mesure_capteur_IR(int pin_IR, int nb_mesure=10);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
  
  //
  // initialisation des variables globales:
  //
  oldStateBP1 = LOW; 
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  pinMode(pinBP1, INPUT);

  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();
   
  // Temporisation démarrage
  tempo_LCD(2);

  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);

  // Initialiser le driver du moteur PàP
  if (!shield.begin()) 
  {
    LCD_display("ERR: Shield PAP", 1);
    STOP    // Arrêter là....
  }
  // Intialize the Shield Driver ADFRUIT:
  LCD_display("Shield PAP OK", 1);

  // Libérer couple moteur:
  myMotorPAP_2->release();          
  
  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //

  stateBP1 = digitalRead(pinBP1);
  int receivedFromMaster = digitalRead(pinReadMaster);
  int receivedFromSlave  = digitalRead(pinReadSlave);
  
  // Lecture capteur présence bougie entrée convoyeur
  int capteur_IR_start = mesure_capteur_IR(pin_capteur_IR_start); 
  
  if (debug)
  {
    String mess = "\nFrom master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "From Slave : " +String(oldReceivedFromSlave) + " " + String(receivedFromSlave);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStateBP1) + " " + String(stateBP1);
    Serial.println(mess);
  }
  
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement Event_BPA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStateBP1 == LOW) && (stateBP1 == HIGH); 

  // Master:GO 
  // L'automate slave-Intermédiaire reçoit un front descendant venant de l'automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);

  // Slave:OK 
  // L'automate Slave-Intermédiaire reçoit un front descendant venant de l'automate-Aval)
  Slave_OK  = (oldReceivedFromSlave  == HIGH) && (receivedFromSlave  == LOW);

  // Événement BD : "Bougie Détectée"
  Event_BD = (capteur_IR_start > 135);
  
  if (Master_GO)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAKE_UP(); 
      break;
    }
  }
  else if (Slave_OK)
  {
    switch (state)
    {
      case WAKE_UP:           
        change_to_state_WAIT();
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH
      break;
    }
  }
  else if (Event_BD)
  {
    switch (state)
    {
      case WAIT:
        change_to_state_RUN();
     break;
    }
  }
  else if (Event_BPA)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAKE_UP();  
      break;

      case WAKE_UP:
        change_to_state_WAIT();
      break;
    }
  }
  else if (Event_DONE)
  {
    switch(state)
    {
      case RUN:
        change_to_state_WAIT();
      break;
    }
    Event_DONE = false;
  }

  //
  // 3 - Traitement des états
  //
  
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAKE_UP:
       // faire clignoter la LEDV
       digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
     ;    // RAF
    break;

    case RUN:
    
      myMotorPAP_2->setSpeed(60);  // 60 tours / min => 1 tours / sec

      Serial.println("Capteur_IR_start: détection bougie \n\t Activation du moteur...");
      while (true)
      {
        int capteur_IR_IA    = mesure_capteur_IR(pin_capteur_IR_IA, 10); 
        int capteur_IR_masse = mesure_capteur_IR(pin_capteur_IR_masse, 10); 
        if (capteur_IR_IA >135)
        {
          // rendu sur capteur IA
          Serial.println("Capteur_IR_IA: détection bougie \n\t Pause moteur attente IA");
	  LCD_display("Bougie IR_IA", 1, false);
          // pause moteur:
          myMotorPAP_2->release();
          // TODO: en attendant que le WP7000_DetectionDéfaut soit opérationnel, on attend un peu
          delay(5000);
          myMotorPAP_2->step(1000, FORWARD, DOUBLE);
        }
        else if (capteur_IR_masse > 135)
        {
          // rendu sur capteur masse
          Serial.println("capteur_IR_masse: détection bougie \n\t Pause moteur");
          LCD_display("Bougie IR_masse", 1, false);
          myMotorPAP_2->release();
          // TODO: en attendant que le WP4000_ContrôleMasse soit opérationnel, on attend un peu
          delay(5000);  
          break;      
        }

        // avancer de 1mm 
        myMotorPAP_2->step(5, FORWARD, DOUBLE);   
      }
      // Générer l'événement DONE
      Event_DONE = true;
    break;
  }

  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldReceivedFromSlave  = receivedFromSlave;
  oldStateBP1 = stateBP1;
 
  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //

  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline int mesure_capteur_IR(int pin_IR, int nb_mesure)
{
  int valeur = 0;
  for (int i=0; i < nb_mesure; i++)
  {
    valeur += analogRead(pin_IR);
    delay(2);
  }
  return int(float(valeur) / float(nb_mesure));
}

inline void change_to_state_WAKE_UP()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //

  state= WAKE_UP;

  // éteindre LED verte
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("WAKE_UP", 0);
  LCD_display("", 1, false);
  delay(1000);
          
  // Envoyer un GO à l'Automate-Aval
  digitalWrite(pinWriteSlave, LOW);
  delay(tau); // ms
  digitalWrite(pinWriteSlave, HIGH);  // keep the line HIGH

  // remettre le clignotement à 1Hz
  tempo = 500;
}

void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAIT
  //
  state = WAIT;

  // Allumer LED verte
  digitalWrite(pinLEDV, HIGH);
  
  // Affichage sur LCD:
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(500);
  
  // tempo sans clignotement
  tempo = 300;
}

void change_to_state_RUN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état RUN
  //
  state = RUN;

  // éteindre LEDV, allumer LEDR
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, HIGH);
  
  // Affichage sur LCD:
  LCD_display("RUN", 0);
  LCD_display("Bougie IR_start", 1, false);
  delay(500);

  // tempo sans clignotement          
  tempo = 300;
}
