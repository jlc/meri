//
// Plateforme MERI
// Automate à États Finis - Slave Final 
// WP9200_Appro/Stockage
// 

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// "Adafruit_MotorShield" pour piloter un moteur pas à pas
#include <Adafruit_MotorShield.h>

// "Servo" pour piloter le servomoteur
#include <Servo.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAIT 2
#define RUN 3
#define MAINTAIN 4
#define BOX 5

#define pinReadMaster  3
#define pinWriteMaster 4

#define pinServo1 5
#define pinServo2 6
#define pinServo3 9
#define pinServoD 10
#define pinServoP 11

#define pinCapteur A1

#define pinLEDV 7
#define pinLEDR 8
#define pinPB1  2

#define STOP while (true) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
// Événements gérés par l'automate
bool Master_GO  = false;
bool Event_BPA  = false;
bool Event_BD   = false;
bool Event_DONE = false;
bool Event_FULL  = false;

const bool debug = false;

int state;                 // L'ETAT de l'automate
int oldStatePB1;           // mémorise l’état du bouton poussoir PB1
int oldReceivedFromMaster; // mémorisation du signal émis par Master
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

//
// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
//
LiquidCrystal_I2C lcd(0x20,16,2);  

struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

int num_colonne;    // le numérpo de la colonne de stockage : 1,2 ou 3
int num_plate;      // le numéro du plateau dans la colonne : 0,1,2 ou 3

// Les 4 servomoteurs :
Servo monServo1;
Servo monServo2;
Servo monServo3;
Servo monServoP;
Servo monServoD;

// angle utiles:
int angle_plateau;  // pour changer d'étage au sein d'une colonne
int angle_colonne;  // pour changer de colonne

// angle dont le servo doit tourner afin de monter les plateaux de 5cm 
const int delta_angle = 50.4; 

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
int mesure_capteur_IR(int pin_IR, int nb_mesure=10);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();
void change_to_state_MAINTAIN();

void setup()
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
   
  //
  // initialisation des variables globales:
  //
  oldStatePB1 = LOW; 
  oldReceivedFromMaster = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  
  pinMode(pinPB1, INPUT);
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  

  // Temporisation démarrage
  tempo_LCD(2);

  // affectation des servomoteurs aux broches 
  monServo1.attach(pinServo1);
  monServo2.attach(pinServo2);
  monServo3.attach(pinServo3);
  monServoP.attach(pinServoP);
  monServoD.attach(pinServoD);

  //
  // L'automate démarre dans l'état INIT
  //  
  state = INIT;
  tempo = 500; //ms, clignotement 1 Hz
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);
  delay(1000);

  num_colonne = 1;
  num_plate   = 3;

  angle_plateau = 2;
  angle_colonne = 2;

  monServo1.write(angle_plateau);
  monServo2.write(angle_plateau);
  monServo3.write(angle_plateau);
  monServoP.write(angle_colonne);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //

  int statePB1    = digitalRead(pinPB1);
  int receivedFromMaster = digitalRead(pinReadMaster);
  int infoCapteur = analogRead (pinCapteur);  

  if (debug)
  {
    String mess = "From master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStatePB1) + " " + String(statePB1);
    Serial.println(mess);
    Serial.println("");
  }

  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Master:GO (Slave-Intermédiaire reçoit un front descendant venant de Automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);
  
  // Événement Event_BPA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStatePB1  == LOW) && (statePB1 == HIGH); 

  // Événement Bougie Détectée:
  Event_BD = (infoCapteur > 300) && (infoCapteur < 500);

  if (Master_GO)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAIT();

        // Send OK to previous Slave:
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH); 

        // Initialiser le système de stockage:
        num_colonne = 1;
        num_plate   = 3;
        angle_plateau = 2;
        angle_colonne = 2;
        monServo1.write(angle_plateau);
        monServoP.write(angle_colonne);        
    break;

    case WAIT:
      change_to_state_BOX();
    break;
    }
  }
  else if (Event_BD) 
  {
    // événement "bougie détectée" 
    LCD_display("Bougie !", 1);
 
    switch(state)
    {
      case WAIT:
        change_to_state_RUN();        
      break;
    }
  }
  else if (Event_FULL)
  {
    switch(state)
    {
      case RUN:
        change_to_state_MAINTAIN();     
      break;
    }
    Event_FULL = false;    
  }
  else if (Event_DONE)
  {
    switch(state)
    {
      case RUN:
        change_to_state_WAIT();     
      break;

      case BOX:
        change_to_state_WAIT();  
      break;
    }
    Event_DONE = false;
  }
  else if (Event_BPA)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAIT();

        // Initialiser le système de stockage:
        num_colonne = 1;
        num_plate   = 3;
        angle_plateau = 2;
        angle_colonne = 2;
        monServo1.write(angle_plateau);
        monServoP.write(angle_colonne);        
      break;

      case WAIT:
        change_to_state_RUN();        
      break;
      
      case MAINTAIN:
        change_to_state_WAIT();
        // Initialiser le système de stockage:
        num_colonne = 1;
        num_plate   = 3;
        angle_plateau = 2;
        angle_colonne = 2;
        monServo1.write(angle_plateau);
        monServoP.write(angle_colonne);  
      break;
    }

  }

 //
  // 3 - Traitement des états
  //
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
      ; // RAF  
    break;

    case RUN:
    {
      if (num_colonne == 1)             
        {
          // Attendre un peu que la bougie aille sur le plateau
          delay(2000);
          
          // préparer le nouvel index plateau:
          num_plate -= 1;
          if (num_plate == -1)
          {
            // Changer de colonne
            num_colonne += 1;
            angle_colonne += 120;
            monServoP.write(angle_colonne);
            // on repart sur le plateau 3 de la nouvelle colonne:
            num_plate = 3;        
          }
          else
          {
            // monter les plateau d'un cran
            angle_plateau += delta_angle;  
            monServo1.write(angle_plateau);
          }
        }
      else if (num_colonne == 2)     
        {
          // Attendre un peu que la bougie aille sur le plateau
          delay(2000);
          
          // préparer le nouvel index plateau:
          num_plate -= 1;

          if (num_plate == -1)
          {
            // Changer de colonne
            num_colonne += 1;
            angle_colonne += 120;
            monServoP.write(angle_colonne);
            // on repart sur le plateau 3 de la nouvelle colonne:
            num_plate = 3;        
          }
          else
          {
            // monter les plateau d'un cran
            angle_plateau += delta_angle;  
            monServo2.write(angle_plateau);
          }

        }
      else if (num_colonne == 3)        
      {
        // Attendre un peu que la bougie aille sur le plateau
        delay(2000);
        
        // préparer le nouvel index plateau:
        num_plate -= 1;

        if (num_plate == -1)
        {
          // Le stockage est plein....
          Event_FULL = true;
        }
        else
        {
          // monter les plateau d'un cran
          angle_plateau += delta_angle;  
          monServo3.write(angle_plateau);
        }
      }
    }
    break;

    case MAINTAIN:
      // faire clignoter la LEDR
      digitalWrite(pinLEDR, 1 - digitalRead(pinLEDR));  
    break;

    case BOX:
      monServoD.write(100);
      delay(500);
      monServoD.write(0);
    break;
  }
  
  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldStatePB1 = statePB1;

  delay(tempo);   
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //

  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline int mesure_capteur_IR(int pin_IR, int nb_mesure)
{
  //
  // faire la moyenne de "nb_mesure" valeurs du capteur IR
  //
  int valeur = 0;
  for (int i=0; i < nb_mesure; i++)
  {
    valeur += analogRead(pin_IR);
    delay(2);
  }
  return int(float(valeur) / float(nb_mesure));
}

inline void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state = WAIT;

  // éteindre/allumer les LEDS
  digitalWrite(pinLEDR, LOW);
  digitalWrite(pinLEDV, HIGH);
  
  // Affichage sur LCD
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(100);
  
  // tempo sans clignotement
  tempo = 300;
}

inline void change_to_state_RUN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state= RUN;
  
  // éteindre/allumer les LEDS
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, HIGH);

  // Affichage sur LCD:
  LCD_display("RUN", 0);
  LCD_display("Bougie en BAS", 1);

}

inline void change_to_state_MAINTAIN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état MAINTAIN
  //
  state = MAINTAIN;

  // éteindre/allumer les LEDS
  digitalWrite(pinLEDR, LOW);
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("MAINTAINANCE  ", 0);
  LCD_display("Vider plateaux", 1);
  delay(500);
  
  // tempo clignotement à 1 Hz
  tempo = 500;
}

inline void change_to_state_BOX()
{
  // 
  // Actions à toujours faire pour la transition vers l'état MAINTAIN
  //
  state = BOX;

  // éteindre/allumer les LEDS
  digitalWrite(pinLEDR, LOW);
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("BOX  ", 0);
  LCD_display("Appro en cours", 1);
  delay(500);
  
  // tempo clignotement à 1 Hz
  tempo = 500;
}