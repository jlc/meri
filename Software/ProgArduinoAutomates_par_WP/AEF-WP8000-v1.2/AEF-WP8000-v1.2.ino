//
// Plateforme MERI
// Automate à États Finis - Slave Intermédiaire
// WP8000_Transport(Tri)
// 

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// Servo.h pour utiliser la bib. Servo pour piloter les servoteurs en PWM
#include <Servo.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAKE_UP 2
#define WAIT 3
#define RUN 4

#define pinLEDV 7
#define pinLEDR 8
#define pinPB1  2

#define pinReadMaster  3
#define pinWriteMaster 4
#define pinReadSlave   6
#define pinWriteSlave  5

#define pinCapteur 9  // Détecteur optique 
#define pinServo 11   // ServoMoteur connecté sur broche 9

#define STOP while (true) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
// Événements gérés par l'automate
bool Master_GO  = false;
bool Slave_OK   = false;
bool Event_BPA  = false;
bool Event_BD   = false;
bool Event_DONE = false;

const bool debug = false;

int state;                 // L'ETAT de l'automate
int oldStateBP1;           // Mémorise l’état du bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int oldReceivedFromSlave;  // Mémorisation du signal émis par Master
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

// Définition de la variable LCD_mess de type LCD_message
struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

//
// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
//
LiquidCrystal_I2C lcd(0x20,16,2);  

Servo monServo;  // déclaration d'un objet servo

int angle = 8;   // angle initial du servomoteur (valeur a changer apres avoir mit le bras)
int oldInfoCapteur;

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
int mesure_capteur_IR(int pin_IR, int nb_mesure=10);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
  
  //
  // initialisation des variables globales:
  //
  oldStateBP1 = HIGH; 
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
  oldInfoCapteur = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  pinMode(pinPB1, INPUT);
  pinMode(pinCapteur, INPUT_PULLUP);
  
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // Barre progression 
  tempo_LCD(2);

  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide

  // éteindre les LEDs:
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, LOW);

  // Affichage sur LCD:
  LCD_display("INIT", 0);

  // attachee le servo à la broche 'pinServo' et le positionner
  monServo.attach(pinServo);  
  monServo.write(angle);

  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  int receivedFromMaster = digitalRead(pinReadMaster);
  int receivedFromSlave  = digitalRead(pinReadSlave);
  int stateBP1 = digitalRead(pinPB1);
  int infoCapteur = digitalRead (pinCapteur);
  String mess = "infoCapteur: " + String(infoCapteur) + "; oldInfoCapteur: " + String(oldInfoCapteur);
  Serial.println(mess);

  if (debug)
  {
    String mess = "From master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "From Slave : " +String(oldReceivedFromSlave) + " " + String(receivedFromSlave);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStateBP1) + " " + String(stateBP1);
    Serial.println(mess);
    Serial.println("");
  }
  
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement Event_PBA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStateBP1 == LOW) && (stateBP1 == HIGH); 

  // Master:GO 
  // L'automate slave-Intermédiaire reçoit un front descendant venant de l'automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);

  // Slave:OK 
  // L'automate Slave-Intermédiaire reçoit un front descendant venant de l'automate-Aval)
  Slave_OK = (oldReceivedFromSlave  == HIGH) && (receivedFromSlave  == LOW);

  // Événement Event_BD : une bougie est détectée sur le capteur de présence
  Event_BD =  (oldInfoCapteur == HIGH) && (infoCapteur == LOW);

  if (Master_GO)  
  {
    switch (state)
    {
      case INIT: 
        change_to_state_WAKE_UP();
      break;
    }
  }  
  else if (Slave_OK)
  {
    switch (state)
    {
      case WAKE_UP:           
        change_to_state_WAIT();
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH
      break;
    }
  }
  else if (Event_BD)
  {
    switch (state)
    {
      case WAIT:
        change_to_state_RUN();
      break;
      //Event_BD = false;
    }
  }
  else if (Event_DONE)
  {
    switch(state)
    {
      case RUN:
        change_to_state_WAIT();
      break;
    }
    Event_DONE = false;
  }
  else if (Event_BPA)
  {
    switch (state)
    {
      case INIT: 
        change_to_state_WAKE_UP();
      break;
      
      case WAKE_UP:           
        change_to_state_WAIT();
      break;
    }    
  }

  //
  // 3 - Traitement des états
  //
  
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAKE_UP:
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
      // Afficher la valeur lue sur le capteur de présence;
      LCD_display("Capteur: " + String(infoCapteur), 1);
    break;
    
    case RUN:
      delay(180); // on attend 2 secondes

      const int end_pos   = 45;  // position angulaire à atteindre
      const int start_pos = 0;   // position angulaire de départ & fin
      
      for (int position = start_pos; position <= end_pos; position++)
      { 
        // positionner le servomoteur sur les 'position' intermédiares:
        monServo.write(position);
        delay(15);  // temporiser 15 millisecondes
      }
      
      // revenir à la position de départ:
      for (int position = end_pos; position > start_pos; position--)
      { 
        monServo.write(position);
        delay(15);  // le bras du servomoteur prend la position de la variable position
      }

      // Générer l'événement DONE:
      Event_DONE = true;   

    break;
  }
  
  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldReceivedFromSlave  = receivedFromSlave;
  oldStateBP1 = stateBP1;
  oldInfoCapteur = infoCapteur;
  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //
  
  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline void change_to_state_WAKE_UP()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state= WAKE_UP;

  // éteindre LED verte
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("WAKE_UP", 0);
  LCD_display("", 1, false);
  delay(1000);
          
  // Envoyer un GO à l'Automate-Aval
  digitalWrite(pinWriteSlave, LOW);
  delay(tau); // ms
  digitalWrite(pinWriteSlave, HIGH);  // keep the line HIGH

  // remettre le clignotement à 1Hz
  tempo = 500;
}

void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state = WAIT;

  // Allumer/éteidre les LEDs
  digitalWrite(pinLEDV, HIGH);
  digitalWrite(pinLEDR, LOW);
  
  // Affichage sur LCD:
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(500);
  
  // tempo sans clignotement
  tempo = 200;
}

void change_to_state_RUN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état RUN
  //
  state = RUN;

  // éteindre LEDV, allumer LEDR
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, HIGH);
  
  // Affichage sur LCD:
  LCD_display("RUN", 0);
  LCD_display("", 1, false);
  delay(500);

  // tempo sans clignotement          
  tempo = 300;
}
  
