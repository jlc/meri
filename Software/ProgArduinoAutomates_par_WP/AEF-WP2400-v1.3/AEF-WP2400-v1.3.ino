//
// Programme de l'automate du WP2400_Démoulage
//

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

// "Adafruit_MotorShield" pour piloter un moteur pas à pas
#include <Adafruit_MotorShield.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAKE_UP 2
#define WAIT 3
#define RUN 4

#define pinReadMaster 3
#define pinWriteMaster 4
#define pinReadSlave 6
#define pinWriteSlave 5
#define pinLEDV 7
#define pinLEDR 8
#define pinBP   A0
#define pinBeamIR 2

#define STOP while (true) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
const bool debug = false;

int state;                // L'ETAT de l'automate
int statePB1;             // état courant Bouton poussoir
int oldStateBP1;          // Mémorisation de l'état du Bouton Poussoir (BP)
int oldReceivedFromSlave; // Mémorisation du signal émis par Slave
int oldReceivedFromMaster;// Mémorisation du signal émis par Master
int receivedFromSlave;
int receivedFromMaster;
int tempo;                // délais de la boucle loop 
int tau = 500;            // durée du front descendant

// Définition de la variable LCD_mess de type LCD_message
struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

Adafruit_MotorShield shield = Adafruit_MotorShield();
const int nb_step_per_revol = 200;
const float degre_per_step = 360./nb_step_per_revol;

Adafruit_StepperMotor * motorZ = shield.getStepper(nb_step_per_revol, 1); // tourner
Adafruit_StepperMotor * motorRot = shield.getStepper(nb_step_per_revol, 2); // descendre

bool Event_DONE = false;

LiquidCrystal_I2C lcd(0x20,16,2); 

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
int mesure_capteur_IR(int pin_IR, int nb_mesure=10);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();
void change_to_state_RUN();

void setup() 
{
  
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
  
  //
  // initialisation des variables globales:
  //
  oldStateBP1 = LOW; 
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
    
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  pinMode(pinBP, INPUT);
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);
  pinMode(pinBeamIR,INPUT_PULLUP);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // Barre progression 
  tempo_LCD(2);

  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide

  // éteindre les LEDs:
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, LOW);

  // Affichage sur LCD:
  LCD_display("INIT", 0);

  // Initialiser le driver du moteur PàP
  if (!shield.begin()) 
  {
    LCD_display("ERR: Shield PAP", 1);
    STOP    // Arrêter là....
  }
  // Intialize the Shield Driver ADFRUIT:
  LCD_display("Shield PAP OK", 1);

  // Libérer couple moteur:
  motorZ->release();
  motorRot->release();          
  
  delay(1000);
}

void loop() 
{

  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int stateBP1 = digitalRead(pinBP);
  int receivedFromMaster = digitalRead(pinReadMaster);
  int receivedFromSlave  = digitalRead(pinReadSlave);

  if (debug)
  {
    String mess = "From master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "From Slave : " +String(oldReceivedFromSlave) + " " + String(receivedFromSlave);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStateBP1) + " " + String(stateBP1);
    Serial.println(mess);
    Serial.println("");
  }


  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement Event_BPA (Bouton Poussoir Appuyé: front montant)
  bool Event_BPA = (oldStateBP1 == LOW) && (stateBP1 == HIGH); 

  // Master:GO (Slave-Intermédiaire reçoit un front descendant venant de Automate-Amont)
  bool Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);

  // Slave_OK (Slave-Intermédiaire reçoit un front descendant venant de Automate-Aval)
  bool Slave_OK  = (oldReceivedFromSlave  == HIGH) && (receivedFromSlave  == LOW);

  if (Master_GO)  
  {
    switch (state)
    {
      case INIT: 
        change_to_state_WAKE_UP();
      break;

      case WAIT: 
        change_to_state_RUN();
      break;
    }
  }
  else if (Slave_OK)
  {
    switch (state)
    {
      case WAKE_UP:           
        change_to_state_WAIT();
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH
      break;
    }
  }
  else if (Event_DONE)
  {
    switch(state)
    {
      case RUN:
        change_to_state_WAIT();
      break;
    }
    Event_DONE = false;
  }
  else if (Event_BPA)
  {
    switch (state)
    {
      case INIT: 
        change_to_state_WAKE_UP();
      break;
      case WAKE_UP:           
        change_to_state_WAIT();
      break;
      case WAIT: 
        change_to_state_RUN();
      break;
    }
  }

  //
  // 3. Traitement des états
  //

  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAKE_UP:
     // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
    // RAF
    break;

    case RUN:

      // Mouvement du bras //
      motorRot->setSpeed(1); 
      int oldBeamIR = digitalRead(pinBeamIR);
      while(true)
      {
        int BeamIR = digitalRead(pinBeamIR);
        String mess = "oldBeamIR: " + String(oldBeamIR) + ", BeamIR: " + String(BeamIR);
        Serial.println(mess);
        if ((oldBeamIR == HIGH) && (BeamIR == LOW))
        {
          break;
        }
        motorRot->step(1, FORWARD, MICROSTEP);
        oldBeamIR == BeamIR;
      } 
      
      motorZ->setSpeed(10000);   // 30 tours / min => 1/2 tour / sec
      motorZ->step(5000, FORWARD, DOUBLE );
      motorZ->release();
      delay(5000);

        motorZ->setSpeed(1000);   // 30 tours / min => 1/2 tour / sec
      motorZ->step(5000, BACKWARD, DOUBLE);
      motorZ->release();
      delay(1000);     
      
      motorRot->setSpeed(1);   
      motorRot->step(100, BACKWARD, MICROSTEP);
      //motorRot->release();
      delay(3000);  
    

      Event_DONE = true;   
    break;
    
  }
  
  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldReceivedFromSlave  = receivedFromSlave;
  oldStateBP1 = stateBP1;
  
  delay(tempo);

}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //
  
  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting 10s...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline void change_to_state_WAKE_UP()
{
  // Changer d'état
  state= WAKE_UP;

  // éteindre LED verte
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("WAKE_UP", 0);
  LCD_display("", 1, false);
  delay(1000);
          
  // Envoyer un GO à l'Automate-Aval
  digitalWrite(pinWriteSlave, LOW);
  delay(tau); // ms
  digitalWrite(pinWriteSlave, HIGH);  // keep the line HIGH

  // remettre le clignotement à 1Hz
  tempo = 500;
}

void change_to_state_WAIT()
{
  // Changer d'état
  state = WAIT;

  // Allumer/éteidre les LEDs
  digitalWrite(pinLEDV, HIGH);
  digitalWrite(pinLEDR, LOW);
  
  // Affichage sur LCD:
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(500);
  
  // tempo sans clignotement
  tempo = 200;
}

void change_to_state_RUN()
{
   // transition d'état            
  state = RUN;

  // éteindre LEDV, allumer LEDR
  digitalWrite(pinLEDV, LOW);
  digitalWrite(pinLEDR, HIGH);
  
  // Affichage sur LCD:
  LCD_display("RUN", 0);
   LCD_display("", 1, false);
  delay(500);

  // tempo sans clignotement          
  tempo = 300;
}




















  
