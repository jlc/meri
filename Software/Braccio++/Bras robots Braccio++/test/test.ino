void setup() 
{
typedef float six_angles[6];

const six_angles WORK_POS[] = \
{{211.0, 158.0, 219.0, 62.0, 106.0, 91.0},
 {169.0, 158.0, 220.0, 64.0, 108.0, 92.0},
 {169.0, 158.0, 159.0, 101., 132.0, 92.0},
 {169.0, 158.0, 154.0, 93.0, 149.0, 150.0},
 {169.0, 158.0, 160.0, 160.0, 130.0, 139.0},
 {169.0, 159.0, 192.0, 87.0, 115.0, 139.0},
 {211.0, 159.0, 193.0, 87.0, 115.0, 139.0}, 
 {-1}};

 
Serial.begin(115200);
Serial.print("WORK_POS[0]: ");
Serial.println(*WORK_POS[8]);

int i = 0;
while (*WORK_POS[i] != -1)
{
  const six_angles & angles = WORK_POS[i++];
  Serial.println(String(angles[0])+", "+String(angles[1])+", "+String(angles[2])+", "+String(angles[3])+", "+String(angles[4])+", "+String(angles[5]));
}
while (true) {;}
}

void loop() {
  // put your main code here, to run repeatedly:
  
}
