//
// Programme pour afficher les angles des 6 moteurs du Braccio 
// quand il est dans une position donnée.
//
// Plateforme MERI - Affiche_angles_Braccio++.ino
//
// v1.0 2023-03-10 Théo Parrinello & Fatima Cruz-Solano : version initiale
// v1.1 2023-03-28 JLC : mise en forme
// v1.2 2023-03-28 JLC : Position Cobra au démarrage
//

// Le but de ce pogramme est de permettre à l'utilisateur de faire afficher 
// dans le terminal Arduino les valeurs des angles des 6 servomoteurs du bras 
// robotisé Braccio++ mis dans des positions successives qui décomposent
// un mouvement complet.
// - un appui sur le bouton ENTER permet d'afficher les valeur des 6 angles
//   de la position courante du Braccio++
// - un appui sur le bouton SELECT du joystick permet de finir l'acquisition.
//
// A la fin de l(acquisition on obtient sur le moniteur Arduino un affichage 
// semblalble à : 
// 
//   //============== Position de travail ========================//
//   const six_angles WORK_POS[] = \
//   {{211.0, 158.0, 219.0, 62.0, 106.0, 91.0},
//   {169.0, 158.0, 220.0, 64.0, 108.0, 92.0},
//   {169.0, 158.0, 159.0, 101., 132.0, 92.0},
//   {169.0, 158.0, 154.0, 93.0, 149.0, 150.0},
//   {169.0, 158.0, 160.0, 160.0, 130.0, 139.0},
//   {169.0, 159.0, 192.0, 87.0, 115.0, 139.0},
//   {211.0, 159.0, 193.0, 87.0, 115.0, 139.0},
//   {-1}};
//   //================= Fin positions ===========================//
//
// Il ne reste plus qu'à copier le code C++ entre les lignes //====...====// et 
// le coller dans le fichier .ino cible.
//

#include <Braccio++.h>

//////////////////////////////////////////
// Les variables globales et les macros //
//////////////////////////////////////////

#define BUTTON_SELECT 3
#define BUTTON_ENTER  6

//
// Braccio ++ joints
//
auto gripper    = Braccio.get(1);
auto wristRoll  = Braccio.get(2);
auto wristPitch = Braccio.get(3);
auto elbow      = Braccio.get(4);
auto shoulder   = Braccio.get(5);
auto base       = Braccio.get(6);

//
// Les positions du bras robotisé
//

// Déclaration du type "six_angle" : tableau de 6 flots
typedef float six_angles[6];

// Position de sécurité, bras vertical:
const six_angles SAFE_POS = {157.5, 157.5, 157.5, 157.5, 157.5, 90.0};


// Position "prêt à travailler" (Cobra):
const six_angles WAIT_POS = {160.0, 160.0, 210.0, 240.0, 100.0, 180.0};

bool movement = false;

float angles[6];

void setup() 
{
  // Attendre le démarrage de la liason série:
  Serial.begin(115200);
  while(!Serial){}

  // Démarrer le Braccio:
  if (Braccio.begin())
  {

    // Mettre le bras en vitesse lente:
    Braccio.speed(speed_grade_t(3000));
    delay(2000);

    // mettre le bras robotisé en position SAFE (aligné vertical):
    Braccio.moveTo(SAFE_POS[0], SAFE_POS[1], SAFE_POS[2], SAFE_POS[3], SAFE_POS[4], SAFE_POS[5]);
    delay(2000);

    // Mettre le bras en position d'attente (Cobra):
    Braccio.moveTo(WAIT_POS[0], WAIT_POS[1], WAIT_POS[2], WAIT_POS[3], WAIT_POS[4], WAIT_POS[5]);
    delay(2000);

    Braccio.disengage();
    delay(500);
    Serial.println("Braccio++ prêt :\n\n");
    Serial.println("  1/ Mettre le bras dans la position voule et appuyer sur ENTER pour faire ");
    Serial.println("     afficher les positions angulaires des 6 moteur du Braccio++.");
    Serial.println("  2/ Répéter 1/ pour toutes les positions du bras qui décomposent le mouvement complet.\n\n");
    Serial.println("À la fin, appuyer sur le joystick-SELECT pour terminer : ");
    Serial.println("le code C++ à copier coller est entre les 2 lignes '//=====...=====//\n\n\n\n");
  }
  else
  {
    Serial.println("PB démarrage Braccio++, please check and reset");
    while (true) {}
  }

  // Afficher l'entête du code C++ qui devra être copié/collé :
  Serial.println("//============== Position de travail ========================//");
  Serial.println("const six_angles WORK_POS[] = {\\\n");
}

void loop() 
{
  int pressedKey = Braccio.getKey();

  if (pressedKey == BUTTON_ENTER)
  {
    Braccio.positions(angles);
    Serial.println(" {"+String(angles[0])+", "+String(angles[1])+", "+String(angles[2])+", " +\
                        String(angles[3])+", "+String(angles[4])+", "+String(angles[5])+"},");
   }
  else if  (pressedKey == BUTTON_SELECT)
  {
    Serial.println(" {-1}};");
    Serial.println("//================= Fin positions ===========================//");
    Serial.println("\n\n\nTu peux maintenant copier/coller le code entre les deux lignes //====...====// ");
  } 

  delay(500);
}
