//
// Programme template pour un automate à états finis 
//
// Plateforme MERI - Automate Slave FINAL Braccio++
//
// v1.0 2023-03-10 Jean-Luc Charles & Théo Parrinello : version initiale
// v1.1 2023-03-27 JLC : mise en forme
// v1.2 2023-03-28 JLC : nouvelles positions de travail + tempo 10s + INPUT_PULLUP
// v1.3 2024-01-15 JLC: Formatage pour WorkShop MERI_AEF
//

//////////////////////////////////////////////////////////////////////
//                         IMPORTANT                                //
//////////////////////////////////////////////////////////////////////
// Sur les Braccio++ le entrées D2,D3,D45 et A0, A1 sont utilisées par 
// le joystick du obot:
// BTN_DOWN  -> 2
// BTN_LEFT  -> 3
// BTN_RIGHT -> 4
// BTN_UP    -> 5
// BTN_SEL   -> A0
// BTN_ENTER -> A1
// Le bus SPI utilise les ports 11, 12 et 13 :
// SPI-CIPO -> D12
// SPI-COPI -> D11
// SPI-SCK  -> D13
//////////////////////////////////////////////////////////////////////

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// Le bras robotisé
#include <Braccio++.h>

// WiFiNINA pour avoir la LED RGB
#include <WiFiNINA.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAIT 2
#define RUN  3

//////////////////////////////////////////////////////////////////////
//                         IMPORTANT                                //
//////////////////////////////////////////////////////////////////////
// 
// On utilise les I/O 3, 4 & 5 qui sont nomalement utilisées
// pour le Joystick du Braccio++ : 
//
// !!! ATTENTION !!! il ne faut pas manipuler le Joystick du Bracccio
//                   sous peine de griller les E/S du RP2040 !!!
//////////////////////////////////////////////////////////////////////

#define pinReadMaster  3
#define pinWriteMaster 4

#define STOP while(1) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
// Événements gérés par l'automate
bool Event_BPA  = false;
bool Master_GO  = false;
bool Event_DONE = false;

int state;                 // L'ETAT de l'automate
int prev_BP_ENTER_state;   // mémorisation état bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int oldReceivedFromSlave;  // Mémorisation du signal émis par Slave
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

int etat_LEDG = 0;         // état de la LED Verte

lv_obj_t* label;
String mess;
lv_style_t style;

//
// Braccio ++ joints
//
auto gripper    = Braccio.get(1);
auto wristRoll  = Braccio.get(2);
auto wristPitch = Braccio.get(3);
auto elbow      = Braccio.get(4);
auto shoulder   = Braccio.get(5);
auto base       = Braccio.get(6);

//
// Les positions du bras robotisé
//

// Déclaration du type "six_angle" : tableau de 6 flots
typedef float six_angles[6];

// Position de sécurité, bras vertical:
const six_angles SAFE_POS = {157.5, 157.5, 157.5, 157.5, 157.5, 90.0};

// Position "prêt à travailler" (Cobra):
const six_angles WAIT_POS = {160.0, 160.0, 210.0, 240.0, 100.0, 180.0};

//============== Position de travail ========================//
const six_angles WORK_POS[] = {\

 {161.28, 158.68, 209.71, 209.40, 127.42, 177.50},
 {161.28, 158.68, 209.71, 220.50, 111.12, 225.38},
 {203.88, 152.15, 209.71, 229.63, 111.12, 283.66},
 {203.88, 152.15, 209.95, 230.90, 87.33, 283.66},
 {-1}};
//================= Fin positions ===========================//

/*
//============== Position de travail ========================//
const six_angles WORK_POS[] = {\
 {158.68, 158.84, 209.71, 269.33, 152.30, 183.72},
 {158.68, 157.97, 209.08, 267.67, 114.03, 183.72},
 {158.68, 157.50, 175.93, 267.99, 114.35, 183.72},
 {211.05, 159.00, 174.59, 268.70, 125.76, 229.24},
 {210.89, 157.50, 118.99, 270.27, 193.17, 229.24},
 {152.62, 159.31, 119.78, 270.11, 192.86, 229.24},
 {152.77, 158.29, 231.21, 211.99, 160.88, 229.24},
 {152.77, 157.66, 171.04, 254.68, 141.75, 186.01},
 {152.77, 157.34, 104.42, 270.43, 195.06, 186.01},
 {211.68, 172.30, 104.26, 270.27, 194.43, 186.01},
 {143.32, 170.34, 262.79, 256.10, 121.20, 186.01},
 {-1}};
//================= Fin positions ===========================//
*/

// Les variables globales propres au WP
// ...

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/

void init_LSC_screen(void);
void update_message(const char* message);

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB

  //
  // Vérifier état Braccio++
  //
  if (!Braccio.begin(init_LSC_screen)) 
  {
    update_message("Error Braccio");
    STOP
  }

  //
  // initialisation des variables globales:
  //
  prev_BP_ENTER_state   = 0;
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
  
  //
  // Configuration des E/S numériques
  //
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
    
  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // Temporisation démarrage
  int wait_delais_sec = 2; // secondes
  mess = "Waiting " + String(wait_delais_sec) + " s";
  update_message(mess.c_str());
  delay(wait_delais_sec*1000);

  // mettre le bras robotisé en position SAFE (aligné vertical):
  Braccio.moveTo(SAFE_POS[0], SAFE_POS[1], SAFE_POS[2], SAFE_POS[3], SAFE_POS[4], SAFE_POS[5]);
  delay(2000);

  //
  // L'automate démarre dans l'état INIT
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide
  
  update_message("INIT");

  // éteindre les LEDs:
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //

  int receivedFromMaster = digitalRead(pinReadMaster);
  int cur_BP_ENTER_state = Braccio.isButtonPressed_ENTER();
  
  // DEBUG_JLC: Serial.println("prev_BP: " + String(prev_BP_ENTER_state) + " - curr_BP: " + String(cur_BP_ENTER_state));

  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement BPA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (prev_BP_ENTER_state == LOW) && (cur_BP_ENTER_state == HIGH);

  // Master:GO (Slave-Intermédiaire reçoit un front descendant venant de Automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);

  if (Master_GO)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAIT();
    
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH); // keep the line HIGH
      break;
    }
  }    
  if (Event_DONE)
  {
    switch (state)
    {
      case RUN:
        change_to_state_WAIT();
      break;
    }
    // désactiver le signal Event_DONE:
    Event_DONE = false;
  }
  else if (Event_BPA)
  {
    switch (state)
    {
      case INIT:
        change_to_state_WAIT();
      break;
      
      case WAIT:
        change_to_state_RUN();
      break;
    }
  }    

  //
  // 3 - Traitement des états
  //
  
  switch (state) 
  {
    case INIT:
      // clignotement LED verte
      etat_LEDG = 1 - etat_LEDG;
      digitalWrite(LEDG, PinStatus(etat_LEDG));
    break;
        
    case WAIT:
      ;
    break;

    case RUN:

      // EXEMPLE : Faire faire au bras les mouvements:
      delay(1000);
      int i = 0;
      while (*WORK_POS[i] != -1)
      {
        const six_angles & angles = WORK_POS[i++];
        Braccio.moveTo(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5]);
        delay(1000);
      }

      // Générer le signal Event_DONE:
      Event_DONE = true;
    break;
  }
  
  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  prev_BP_ENTER_state   = cur_BP_ENTER_state;
  
  delay(tempo);
}

void init_LSC_screen(void) 
{
  //
  // Initialisation de l'écran du Braccio++
  //
  Braccio.lvgl_lock();
  lv_style_init(&style);
  lv_style_set_text_font(&style, &lv_font_montserrat_32);  //lv_font_montserrat_48);
  label = lv_label_create(lv_scr_act());
  Braccio.lvgl_unlock();
}

void update_message(const char* message) 
{
  //
  // écrire un message sur l'écran du Braccio++
  //
  Braccio.lvgl_lock();
  mess = message;
  lv_label_set_text_static(label, mess.c_str());
  lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);
  lv_obj_add_style(label, &style, LV_PART_MAIN);
  Braccio.lvgl_unlock();
}

inline void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAIT
  //

  // transition d'état            
  state = WAIT;

  // Allumer LED verte:
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, HIGH);
  digitalWrite(LEDB, LOW);

  // Affichage sur LCD:
  update_message("WAIT");
  delay(500);
  
  // Mettre le bras en position d'attente (Cobra):
  Braccio.moveTo(WAIT_POS[0], WAIT_POS[1], WAIT_POS[2], WAIT_POS[3], WAIT_POS[4], WAIT_POS[5]);
  delay(1000);

  // tempo sans clignotement
  tempo = 300;
}  

inline void change_to_state_RUN()
{
  // 
  // Actions à toujours faire pour la transition vers l'état RUN
  //

  // transition d'état            
  state = RUN;
  
  // Allumer LED rouge:
  digitalWrite(LEDR, HIGH);
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDB, LOW);
  
  // Affichage sur LCD:
  update_message("RUN");
  
  // tempo sans clignotement
  tempo = 300;  
}
