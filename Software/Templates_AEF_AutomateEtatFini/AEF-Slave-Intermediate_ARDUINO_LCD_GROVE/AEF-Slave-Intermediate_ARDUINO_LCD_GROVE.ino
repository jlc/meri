//
// Programme template pour un automate à états finis 
//
// Plateforme MERI - Automate Slave Intermédiaire.
//
// v1.0 2023-03-09 Jean-Luc Charles - Version initiale
// v1.1 2023-03-17 Ajout infos debug
// v1.2 2023-03-28 JLC: tempo 10s +  les entrées en INPUT_PULLUP
// v1.3 2023-06-01 JLC: inversion états BP -> Grove: HIGH when pressed, LOW when released
// v1.4 2023-07-12 JLC: Ajout fonction LCD_Display
// v1.5 2024-01-15 JLC: Formatage pour WorkShop MERI_AEF
// v1.6 2024-02-10 JLC: if ... else if... pour les transitions d'états
//                      Fonctions pour les traitements des états
//

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT    1
#define WAKE_UP 2
#define WAIT    3

#define pinLEDV 7
#define pinBP1  2

#define pinReadMaster  3
#define pinWriteMaster 4
#define pinReadSlave   6
#define pinWriteSlave  5

#define STOP while(1) {;}

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
// Événements gérés par l'automate
bool Event_BPA = false;
bool Master_GO = false;
bool Slave_OK = false;
bool Event_DONE = false;
bool Event_BD   = false;

const bool debug = false;

int state;                 // L'ETAT de l'automate
int oldStateBP1;           // Mémorise l’état du bouton poussoir
int oldReceivedFromMaster; // Mémorisation du signal émis par Master
int oldReceivedFromSlave;  // Mémorisation du signal émis par Slave
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
LiquidCrystal_I2C lcd(0x20,16,2);  

// Les variables globales propres au WP
// ...

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);

// Fonctions pour les transition d'état
void change_to_state_WAKE_UP();
void change_to_state_WAIT();

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
  
  //
  // initialisation des variables globales:
  //
  oldStateBP1 = LOW; 
  oldReceivedFromMaster = HIGH;
  oldReceivedFromSlave  = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinBP1, INPUT);

  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  
  
  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // Temporisation démarrage
  tempo_LCD(3);

  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide

  // éteindre les LEDs:
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);
  
  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int stateBP1 = digitalRead(pinBP1);
  int receivedFromMaster = digitalRead(pinReadMaster);
  int receivedFromSlave  = digitalRead(pinReadSlave);

  if (debug)
  {
    String mess = "\nFrom master: " +String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = "From Slave : " +String(oldReceivedFromSlave) + " " + String(receivedFromSlave);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStateBP1) + " " + String(stateBP1);
    Serial.println(mess);
  }
  
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement Event_PBA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStateBP1 == LOW) && (stateBP1 == HIGH); 

  // Master:GO 
  // L'automate slave-Intermédiaire reçoit un front descendant venant de l'automate-Amont)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);

  // Slave:OK 
  // L'automate Slave-Intermédiaire reçoit un front descendant venant de l'automate-Aval)
  Slave_OK  = (oldReceivedFromSlave  == HIGH) && (receivedFromSlave  == LOW);
  
  if (Master_GO)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAKE_UP();
      break;
    }
  }
  else if (Slave_OK)
  {
    switch (state)
    {
      case WAKE_UP:        
        change_to_state_WAIT();
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH        
      break;
    }
  }
  else if (Event_BPA)
  {
    switch(state)
    {
      case INIT:
        change_to_state_WAKE_UP();  
      break;

      case WAKE_UP:
        change_to_state_WAIT();
      break;
    }
  }

  //
  // 3 - Traitement des états
  //
  
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAKE_UP:
       // faire clignoter la LEDV
       digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
     ;    // RAF
    break;
  }

  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldReceivedFromSlave  = receivedFromSlave;
  oldStateBP1 = stateBP1;
 
  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //
  
  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}

inline void change_to_state_WAKE_UP()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state= WAKE_UP;

  // éteindre LED verte
  digitalWrite(pinLEDV, LOW);
  
  // Affichage sur LCD
  LCD_display("WAKE_UP", 0);
  LCD_display("", 1, false);
  delay(1000);
          
  // Envoyer un GO à l'Automate-Aval
  digitalWrite(pinWriteSlave, LOW);
  delay(tau); // ms
  digitalWrite(pinWriteSlave, HIGH);  // keep the line HIGH

  // remettre le clignotement à 1Hz
  tempo = 500;
}

void change_to_state_WAIT()
{
  // 
  // Actions à toujours faire pour la transition vers l'état WAKE_UP
  //
  state = WAIT;

  // Allumer/éteidre les LEDs
  digitalWrite(pinLEDV, HIGH);
  
  // Affichage sur LCD:
  LCD_display("WAIT", 0);
  LCD_display("", 1, false);
  delay(100);
  
  // tempo sans clignotement
  tempo = 300;
}



