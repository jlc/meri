//
// Programme template pour un automate à états finis 
//
// Plateforme MERI - Automate Slave Final.
//
// v1.0 2023-03-09 Jean-Luc Charles - Version initiale
// v1.1 2023-03-17 Ajout infos debug
// v1.2 2023-03-28 JLC: tempo 10s +  les entrées en INPUT_PULLUP
// v1.3 2023-06-01 JLC: inversion états BP -> Grove: HIGH when pressed, LOW when released
// v1.4 2023-07-12 JLC: Ajout fonction LCD_Display
// v1.5 2024-01-15 JLC: Formatage pour WorkShop MERI_AEF
//

/**********************************************************************
  Les includes nécessaires au programme  
 **********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

/**********************************************************************
  Définition des Macros utiles : états automate, numéros des broches...
 **********************************************************************/
#define INIT 1
#define WAIT 2

#define pinLEDV 7
#define pinPB1  2

#define pinReadMaster  3
#define pinWriteMaster 4

#define STOP while(1) {;}

/**********************************************************************
  Déclaration/définition des variables globales 
 **********************************************************************/
// Événements gérés par l'automate
bool Event_BPA = false;
bool Master_GO = false;

const bool debug = true;

int state;                 // L'ETAT de l'automate
int oldStatePB1;           // mémorise l’état du bouton poussoir PB1
int oldReceivedFromMaster; // mémorisation du signal émis par Master
int tempo;                 // délais de la boucle loop 
int tau = 500;             // durée du front descendant

struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
LiquidCrystal_I2C lcd(0x20,16,2);  

// Les variables globales propres au WP
// ...

/**********************************************************************
  Déclaration des fonctions utiliées dans le programme 
  (la définition est codée à la fin du programme)
 **********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
  
  //
  // initialisation des variables globales:
  //
  oldStatePB1 = LOW; 
  oldReceivedFromMaster = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinPB1, INPUT);
  pinMode(pinReadMaster, INPUT_PULLUP);
  pinMode(pinWriteMaster, OUTPUT);

  // maintient de la sortie Master à HIGH:
  digitalWrite(pinWriteMaster, HIGH);  

  // Initialisation LCD:
  lcd.init();                      
  lcd.backlight();

  // Temporisation démarrage
  tempo_LCD(10);

  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 500; // ms, clignotement lent
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);
  
  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int statePB1 = digitalRead(pinPB1);
  int receivedFromMaster = digitalRead(pinReadMaster);

  if (debug)
  {
    String mess = String(oldReceivedFromMaster) + " " + String(receivedFromMaster);
    Serial.println(mess);
    mess = String(oldStatePB1) + " " + String(statePB1);
    Serial.println(mess);
    Serial.println("");
  }
  
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //

  // Événement BPA (Bouton Poussoir Appuyé: front montant)
  Event_BPA = (oldStatePB1 == LOW) && (statePB1 == HIGH); 
  
  // Master:GO (Slave reçoit un front descendant venant de Master)
  Master_GO = (oldReceivedFromMaster == HIGH) && (receivedFromMaster == LOW);
  
  if (Master_GO || Event_BPA)
  {
    switch(state)
    {
      case INIT:
        // transition d'état
        state= WAIT;
                 
        digitalWrite(pinLEDV, LOW);
        digitalWrite(pinLEDV, HIGH);

        // Affichage sur LCD:
        LCD_display("WAIT", 0);
        LCD_display("", 1);
        // attendre un peu avant d'envoyet OK au master 
        // pour avoir le temps de voir les choses...
        delay(1000);
        
        // envoyer OK à l'automate Amont (Master ou Slave Intermédiaire)
        digitalWrite(pinWriteMaster, LOW);
        delay(tau); // ms
        digitalWrite(pinWriteMaster, HIGH);  // keep the line HIGH
        
        LCD_display("", 1);
        tempo = 300;
     break;
    }
  }
 
  //
  // 3 - Traitement des états
  //
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case WAIT:
      ; // RAF  
    break;
  }

  //
  // 4 - Finir
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldReceivedFromMaster = receivedFromMaster;
  oldStatePB1 = statePB1;

  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //

  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting " + String(nb_sec) + "sec ...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}
