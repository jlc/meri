////////////////////////////////////////////////////////////////////////////////////////
// Platforme MERI:Structure d'un programme pour construire un Automate à États Finis ///
////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright Jean-Luc CHARLES (JLC) - jean-luc.charles@mailo.com
//
// v1.0 2024-01-15 JLC - Version initiale
//

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/

// "Wire": gestion du bus I2C:
#include <Wire.h>         

// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>
...

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAIT 2
...
#define pinBP1 5    
...
#define pinLEDV 8
...

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/

int etat;               // l'état courant de l'automate
int oldStateBP1;        // Mémorisation état du Bouton Poussoir 1 (BP1)
int tempo;              // délais de la boucle loop 
...

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);
...

void setup() 
{  
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB

  //
  // initialisation des variables globales:
  //
  oldStatePB1 = HIGH; 
  ...
  
  //
  // Configuration des E/S numériques
  //
  pinMode(pinBP1, INPUT);
  ...
  pinMode(pinLEDV, OUTPUT);
  ...

  //
  // L'automate démarre dans l'état INIT :
  //  
  etat = INIT;
  ...
}

void loop() 
{  
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  ...

  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //
  ...

  //
  // 3 - Traitement des états de l'automate
  //     
  ...
  switch (etat)
  {
    case INIT:
      ...

    break;
    
    case WAIT:
      ...
      
    break;
    ...
    
  }

  //
  // 4 - Actions à faire à la fin de la boucle loop :
  //
  
  // mémoriser la valeur de certaines variables pour le prochain tour de loop():
  old_variable = current_variable;  
  ...
  
  delay(tempo);
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - serialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //
  ...  
  
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  ...
  
}
