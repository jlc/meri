//
// Programme template pour un automate à états finis 
//
// Plateforme MERI - Automate Master.
//
// v1.0 2023-03-09 Jean-Luc Charles - Version initiale
// v1.1 2023-03-17 Ajout infos debug
// v1.2 2023-03-28 JLC: tempo 10s +  les entrées en INPUT_PULLUP
// v1.3 2023-07-12 JLC: Ajout fonction LCD_Display
// v1.4 2024-01-15 JLC: Formatage pour WorkShop MERI_AEF
//

/**********************************************************************
/ Les includes nécessaires au programme  
/**********************************************************************/
// "Wire": gestion du bus I2C:
#include <Wire.h>       
  
// "LiquidCrystal_I2C": pilotage écran LCD sur bus I2C:
#include <LiquidCrystal_I2C.h>

/**********************************************************************
/ Définition des Macros utiles : états automate, numéros des broches...
/**********************************************************************/
#define INIT 1
#define WAKE_UP 2
#define RUN 3

#define pinReadSlave 4
#define pinWriteSlave 3
#define pinLEDV 5
#define pinLEDR 6
#define pinBP   7

/**********************************************************************
/ Déclaration/définition des variables globales 
/**********************************************************************/
const bool debug = false;

int state;                // L'ETAT de l'automate
int oldStatePB1;          // Mémorisation de l'état du Bouton Poussoir (BP)
int oldReceivedFromSlave; // Mémorisation du signal émis par Slave
int tempo;                // délais de la boucle loop 
int tau = 500;            // durée du front descendant

// Définition de la variable LCD_mess de type LCD_message
struct LCD_message
{
  String line0;
  String line1;
} LCD_mess;

// objet lcd: Afficheur LCD, 2 x lignes x 16 caractères à l'adresse I2C 0x20:
LiquidCrystal_I2C lcd(0x20,16,2);  

// Les variables globales propres au WP
// ...

/**********************************************************************
/  Déclaration des fonctions utiliées dans le programme 
/  (la définition est codée à la fin du programme)
/**********************************************************************/
void LCD_display(const String & mess, int line, bool serialprint=true);
void tempo_LCD(int nb_secondes);

void setup() 
{
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran
  // via le moniteur:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets par la liaison USB
   
  //
  // initialisation des variables globales:
  //
  oldStatePB1 = HIGH; 
  oldReceivedFromSlave = HIGH;
  
  //
  // Configuration des E/S numériques:
  //
  pinMode(pinLEDV, OUTPUT);
  pinMode(pinLEDR, OUTPUT);
  pinMode(pinBP, INPUT);
  pinMode(pinReadSlave, INPUT_PULLUP);
  pinMode(pinWriteSlave, OUTPUT);

  // maintient de la sortie Slave à HIGH:
  digitalWrite(pinWriteSlave, HIGH);  
  
  // Initialisation de l'afficheur LCD:
  lcd.init();                      
  lcd.backlight();

  // Temporisation démarrage
  tempo_LCD(10);
  
  //
  // L'automate démarre dans l'état INIT :
  //  
  state = INIT;
  tempo = 100; //ms, clignotement rapide
  
  // Affichage sur LCD:
  LCD_display("INIT", 0);
  
  delay(1000);
}

void loop() 
{
  //
  // 1 – Lecture périphériques/signaux pouvant provoquer une transition d’état
  //
  
  int statePB1 = digitalRead(pinBP);
  int receivedFromSlave = digitalRead(pinReadSlave);

  if (debug)
  {
    String mess = "From Slave : " +String(oldReceivedFromSlave) + " " + String(receivedFromSlave);
    Serial.println(mess);
    mess = "BP         : "  + String(oldStatePB1) + " " + String(statePB1);
    Serial.println(mess);
    Serial.println("");
  }
    
  //
  // 2 - Traitement des transitions d'état (cf Tableau ‘transitions d'état’)    
  //
  
  // Événement BPA (Bouton Poussoir Appuyé: front descendant)
  bool BPA = (oldStatePB1  == HIGH) && (statePB1 == LOW); 
  
  // évenement Ready (Slave envoit un front descendant)
  bool SLAVE_OK = (oldReceivedFromSlave == HIGH) && (receivedFromSlave == LOW);

  if (BPA)  
  {
    switch (state)
    {
      case INIT: 
        // transition d'état
        state = WAKE_UP;   
        
        // Affichage sur LCD:
        LCD_display("WAKE_UP", 0);
        LCD_display("Send GO > Slave", 1);
        delay(3000);
        
        // Envoi du signal GO (front descendant, durée 'tau') vers Slave:
        digitalWrite(pinWriteSlave, LOW);
        delay(tau);  // ms
        digitalWrite(pinWriteSlave, HIGH);   // keep the line HIGH
        LCD_display(" ", 1);
        
        tempo = 500; //ms, clignotement 1 Hz
      break;
      
      case WAKE_UP:
        SLAVE_OK = true;  // simuler l'événement SLAVE_OK
      break;
    }
  }
  
  if (SLAVE_OK)  
  {
    switch (state)
    {
      case WAKE_UP:
        // transition d'état
        state = RUN;
        
        digitalWrite(pinLEDV, LOW);
        digitalWrite(pinLEDR, HIGH);

        // Affichage sur LCD:
        LCD_display("RUN", 0);
        LCD_display("OK from Slave", 1);
        delay(3000);

        LCD_display(" ", 1);
        tempo = 300; //ms
      break;
    }
  }

  //
  // 3 - Traitement des états de l'automate
  //
  
  switch (state)
  {
    case INIT:  
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;
    
    case WAKE_UP:
      // faire clignoter la LEDV
      digitalWrite(pinLEDV, 1 - digitalRead(pinLEDV));  
    break;

    case RUN:
      ;  // Faire des choses...
    break;
  }

  //
  // 4 - Actions à faire à la fin de la boucle loop :
  //

  // mémoriser ce qui doit l’être, pour le prochain tour de boucle:
  oldStatePB1 = statePB1;
  oldReceivedFromSlave= receivedFromSlave;
 
  delay(tempo); 
}

void LCD_display(const String & mess, int line, bool serialprint)
{
  //
  // Afficher un message sur une ligne (N°0 ou 1) de l'afficheur LCD.
  // La ligne est complétée par des espaces jusqu'au 16 caractères.
  // - mess: le message à afficher.
  // - line: N° de ligne, 0 ou 1.
  // - srialprint: booléen (défaut: true) pour afficher 'mess' aussi sur Serial. 
  //
  
  int L = mess.length();
  const String blanks_16 = "                ";
  String mess_16;
  if (L <16)
    mess_16 = mess + blanks_16.substring(L);
  else 
    mess_16 = mess.substring(0,16);
  
  if (line == 1)
  {
    LCD_mess.line1 = mess_16;
    lcd.setCursor(0,1);
    lcd.print(LCD_mess.line1);
  }
  else if (line == 0)
  {
    LCD_mess.line0 = mess_16;
    lcd.setCursor(0,0);
    lcd.print(LCD_mess.line0);
  }
  if (serialprint) Serial.println(mess_16);
}

void tempo_LCD(int nb_sec)
{
  //
  // Affiche une barre de point jusqu'à nb secondes (inférieur à 14s)
  //
  LCD_display("Waiting 10s...", 0);
  LCD_display(" ", 1);
  lcd.setCursor(0, 1); lcd.print(">");
  lcd.setCursor(nb_sec+1, 1); lcd.print("<");
  delay(1000);
  for (int i=1; i <= nb_sec; i++)
  {
    lcd.setCursor(i % 16, 1);
    lcd.print(".");
    delay(1000);
  }
  LCD_display(" ", 1);
}
