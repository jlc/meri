//
// Programme template pour un automate à états finis - Plateforme MERI
//
// Jean-Luc Charles 2023-03-09 v1.0
//

// variables globales:
int etat;               // l'état courant de l'automate
...

// Définition  des Macros utiles : états automate, numéros des broches...
#define INIT 1
#define WAIT 2
...
#define pinBP1 5
...
#define pinLEDV 8
...

void setup() 
{  
  // Initialisation de l'objet 'Serial' pour afficher des messages à l'écran:
  Serial.begin(9600);    // 9600 : vitesse de tranfert des octets
                         // ARDUINO -> PC par liaison USB
  //
  // Configuration des E/S numériques
  //
  pinMode(pinBP1, INPUT);
  ...
  pinMode(pinLEDV, OUTPUT);
  ...

  //
  // L'automate démarre dans l'état INIT :
  //  
  etat = INIT;
  ...
}

void loop() 
{  
  //
  // 0 - Définition des variables locales utiles...
  //
  int tempo = 200;
  ...
  
  //
  // 1 - Lecture des périphériques/signaux qui peuvent fournir un événement
  //     susceptible de provoquer un changement d'état de l'automate.…   
  ...


  //
  // 2 - Traitement des événements entraînant une transition d'état
  //     (cf Tableau des transitions d'état)
  ...

  
  //
  // 3 - Traitement des états
  //     (cf Tableau des actions des états)
  ...
  
  switch (etat)
  {
    case INIT:
      ...
      ...
    break;
    
    case WAIT:
      ...
      ...
    break;
  }

  //
  // 4 - Actions à faire à la fin de la boucle loop :
  //
  ...
  delay(tempo);
}
